﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AutoFillPaletts))]
public class AutoFillPalettsEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        AutoFillPaletts myScript = (AutoFillPaletts)target;

        EditorGUILayout.LabelField("Placeholder Tests");

        if (GUILayout.Button("Add Box Overlapless"))
        {
            myScript.AddOverlappless();
        }

        if (GUILayout.Button("Add CHAIR Overlapless"))
        {
            myScript.AddOverlapplessChair();
        }

        if (GUILayout.Button("Delete Overlapless Box - LIFO"))
        {
            myScript.TestOverlapDelete();
        }

        EditorGUILayout.LabelField("Shelf Tests");

        if (GUILayout.Button("Filled"))
        {
            myScript.GetPercentageFillShelf();
        }

        if (GUILayout.Button("Add Box At"))
        {
            myScript.AddAnotherShelfBox();
        }

        if (GUILayout.Button("Delete Box At"))
        {
            myScript.DeleteShelfBox(); ;
        }

        EditorGUILayout.LabelField("Size & Corner points coordinates");

        if (GUILayout.Button("Calculate Shelf Size"))
        {
            myScript.CalculateStorageSize();
        }
        
        if (GUILayout.Button("Calculate corner points"))
        {
            myScript.CalculateCornerPoints();
        }
    }
}
