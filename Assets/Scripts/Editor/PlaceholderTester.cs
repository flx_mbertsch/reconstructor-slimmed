﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlaceholderTester : EditorWindow
{
    public GameObject currentSelectedStorage;
    Event e;
    PlaceholderGenerator phGenerator;

    //selected Storage Options
    private int optColumnCount;
    private int optRowCount;
    private Vector3 optOffset;
    private Vector3 optPlaceholderScaling;
    private bool optUseMeshAnchors;
    private bool optholderWorldUP;

    [MenuItem("Tools/PlaceholderTester")]
    public static void ShowWindow()
    {
        //restrict windows resizing
        EditorWindow.GetWindowWithRect(typeof(PlaceholderTester), new Rect(0, 0, 400, 200));
    }

    private void OnSceneGUI(SceneView sceneView)
    {
        e = Event.current;
        if (e.type == EventType.MouseUp && e.button == 1)
        {
            currentSelectedStorage = Selection.activeGameObject;
            //Debug.Log("current " + currentSelectedStorage?.name);
            //refresh the GUI after every click
            Repaint();
        }
    }

    private void OnGUI()
    {
        EditorGUILayout.ObjectField("Ausgewähltes Lager", currentSelectedStorage, typeof(GameObject), true);

        if (currentSelectedStorage != null)
        {
            phGenerator = currentSelectedStorage.GetComponent<PlaceholderGenerator>();
        }
        GUILayout.Label("Spezial Optionen", EditorStyles.boldLabel);

        //only activate the toggle if the object has a way to generate placeholder and is a free place storage
        if (phGenerator != null)
            PlaceholderOptions(phGenerator);

        EditorGUI.EndDisabledGroup();
    }

    //for OnSceneGUI to work
    void OnFocus()
    {
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI; // Just in case
        SceneView.onSceneGUIDelegate += this.OnSceneGUI;
    }

    void OnDestroy()
    {
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
    }

    private void PlaceholderOptions(PlaceholderGenerator _phGenerator)
    {
        EditorGUI.indentLevel++;
        EditorGUI.indentLevel++;

        //TODO: KLasse/interface hierfür
        //hand over values from the current selected storage
        optColumnCount = _phGenerator.columnCount;
        optRowCount = _phGenerator.rowCount;
        optOffset = _phGenerator.offset;
        optPlaceholderScaling = _phGenerator.placeholderScaling;
        optUseMeshAnchors = _phGenerator.useMeshAnchors;
        optholderWorldUP = _phGenerator.placeholderWorldUP;


        optColumnCount = EditorGUILayout.IntField("Anzahl Ebenen", optColumnCount);
        optRowCount = EditorGUILayout.IntField("Anzahl Plätze pro Reihe", optRowCount);
        optOffset = EditorGUILayout.Vector3Field("Versatz", optOffset);
        optPlaceholderScaling = EditorGUILayout.Vector3Field("Platzhalterskalierung", optPlaceholderScaling);
        optUseMeshAnchors = EditorGUILayout.ToggleLeft("Beachte Regalstangen für Platzhalter", optUseMeshAnchors);
        optholderWorldUP = EditorGUILayout.ToggleLeft("Objekte zeigen unabhängig des Regals immer nach oben", optholderWorldUP);

        if (GUILayout.Button("Generiere Lagerplätze"))
        {
            //TODO: Mehr falsche input values abfangen
            if (optColumnCount != 0 && optRowCount != 0)
                _phGenerator.PlaceholderParameterSetterGUI(optColumnCount, optRowCount, optOffset, optPlaceholderScaling, optUseMeshAnchors, optholderWorldUP);

            _phGenerator.CreateStorages();
            _phGenerator.isRNDFilled = false;
        }

        EditorGUI.indentLevel--;

    }
}
