﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

//Required: Layer 9,10,11 - Tag "SingleSpawn"
// layer add to RNDobjekte nicht vergessen, boxcollider definieren

//TODO: Objekttypenliste (Preview grid) gewählte Objekte selektiert lassen, damit nur eine art von Objekten in die Liste kann
//TODO: mouse event über placeholder abfangen und dann Objekte darin korrekt platzieren?
//TODO: Beim Befüllen Ladebalken anzeigen wie viele befüllt/übrig sind
//TODO: Regal Auflagestangen als Fläche machen

//handles the whole GUI window for shelf generation/placeholder filling
public class StorageTool : EditorWindow
{
    public GameObject currentSelectedStorage;
    public int currentSelectedLayer;
    private Color selectionColor = Color.magenta;

    private GameObject parentStorage;
    private Vector2 scrollPosition = Vector2.zero;
    private int incrementID = 0;
    //for placing objects
    private bool setShelfMode = false;
    Vector3 mouseCenter;
    private bool toggleShowCol;
    private Vector3 dragStartPos;
    private bool dragging = false;
    private Vector3 direction;
    private Vector3 middle = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    Vector3[] corners = null;

    //float between 0 and 1, returns false if its below 0.5 and true if above 0.5
    private AnimBool storageAnim;
    private AnimBool advancedAnim;
    private AnimBool phOptionsAnim;
    private AnimBool randomizerAnim;
    private AnimBool folderObjectAnim;
    private AnimBool standardShelfAnim;

    //TODO: Eine Oberklasse/Interface mit allen values und ensptrechende Instanzen herausziehen für die benötigten Optionswerte
    //Default values when opening window
    //shelf default Options
    private int shelfColumnCount = 5;
    private int shelfRowCount = 14;
    private Vector3 shelfOffset = new Vector3(-1, 0, 0);
    private Vector3 shelfPlaceholderScaling = Vector3.one;
    //Highbay default Options
    private float hbHeight = 8;
    private int hbColumnCount = 4;
    private int hbRowCount = 8;
    private Vector3 hbPlaceholderScaling = Vector3.one;
    private bool hbUseMeshAnchors = true;
    //Highbay advanced Options
    private int hbBetweenBarCount = 2;
    private float hbPoleThickness = 0.4f;
    private float hbBarThickness = 0.2f;
    private float hbBarHeight = 0.2f;
    private Material hbHighbayMaterial;
    private Material hbBarMaterial;
    private Material hbCrossMaterial;
    //Place default Options
    private int placeColumnCount = 4;
    private int placeRowCount = 8;
    private Vector3 placeOffset = Vector3.zero;
    private Vector3 placePlaceholderScaling = new Vector3(0.8f, 0.8f, 1);
    private bool placeholderWorldUP = true;
    //selected Storage Options
    private int optColumnCount;
    private int optRowCount;
    private Vector3 optOffset;
    private Vector3 optPlaceholderScaling;
    private bool optUseMeshAnchors;
    private bool optholderWorldUP;

    //RandomFill Options for minmax Slider
    private int spawnPercentage = 60;
    private float minQuantitySpawnValue = 5f;
    private float maxQuantitySpawnValue = 20f;
    private int minQuantitySpawnLimit = 1;
    private int maxQuantitySpawnLimit = 50;
    private bool randomizeRotation = false;
    private int rndNumberRange = 0;

    //ObjectTypes
    private int selectedObjectsTypes = 0;
    private string[] objFolderPaths;
    private string[] fillFolderNames;
    private string currentFolderName;
    private int[] objectTypes = { 0, 1 };

    //list containing the available prefabs.
    [SerializeField]
    private List<GameObject> storageobjects = new List<GameObject>();
    [SerializeField]
    private List<GameObject> standardShelfs = new List<GameObject>();
    [SerializeField]
    private List<GameObject> fillFolderObjects = new List<GameObject>();
    [SerializeField]
    private List<GameObject> choosenRndObjects = new List<GameObject>();
    private GameObject worldObject;

    //Unity folder for Editor assets
    private const string path = "Assets/ObjectKatalog/ShelfTool";
    private const string subPath = "FillObjects";
    private const string parentString = "Lagerobjekte";
    LayerMask layerShelf = 9;
    LayerMask layerPlaceholder = 10;
    LayerMask layerHU = 11;

    [SerializeField]
    private int storageIndex;
    [SerializeField]
    private int standardShelfIndex;
    [SerializeField]
    private int fillObjectsIndex;

    //type variables
    private int hbID;
    private PlaceholderGenerator phGenerator;
    private StorageScript storageScript;
    private Event e;

    //create in the menu bar a tab to open this class as a window
    [MenuItem("Tools/Regalverwalter")]
    public static void ShowWindow()
    {
        //restrict windows resizing
        EditorWindow.GetWindowWithRect(typeof(StorageTool), new Rect(0, 0, 500, 500));
    }

    //called when the window first opens up
    private void OnEnable()
    {
        parentStorage = GameObject.Find(parentString);

        fillFolderNames = GetAllFolderNames(path + "/" + subPath).ToArray();

        //toggle fadegroup to off
        storageAnim = new AnimBool(false);
        //everytime the check value has changed, redraw what is sitting inside the fade group, or else stuttering
        storageAnim.valueChanged.AddListener(Repaint);

        //TODO: Möglihkeit finden, dass nicht jede toggle Animation ihren eignen listener braucht
        advancedAnim = new AnimBool(false);
        advancedAnim.valueChanged.AddListener(Repaint);

        phOptionsAnim = new AnimBool(false);
        phOptionsAnim.valueChanged.AddListener(Repaint);

        randomizerAnim = new AnimBool(false);
        randomizerAnim.valueChanged.AddListener(Repaint);

        folderObjectAnim = new AnimBool(false);
        folderObjectAnim.valueChanged.AddListener(Repaint);

        standardShelfAnim = new AnimBool(false);
        standardShelfAnim.valueChanged.AddListener(Repaint);

        RefreshGridPalette();
    }

    //Does the rendering of the map editor in the scene view.
    private void OnSceneGUI(SceneView sceneView)
    {
        e = Event.current;

        //Get the mouse position and spawn the object there
        if (setShelfMode)
        {
            ShelfModes();

            // Refresh the view
            //sceneView.Repaint();
        }
        else
            LayerInfo();
    }

    //for OnSceneGUI to work
    void OnFocus()
    {
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI; // Just in case
        SceneView.onSceneGUIDelegate += this.OnSceneGUI;
    }

    //everytime something get selected, get the gameobject
    private void OnSelectionChange()
    {
        RevertSelectionColor();
        GetActiveObject();
        Repaint();
    }

    void OnDestroy()
    {
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
    }

    //Editor drawings
    private void OnGUI()
    {
        //Scrollbar
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUILayout.Width(500), GUILayout.Height(500));
        GUILayout.Label("Regalverwalter", EditorStyles.boldLabel);
        EditorGUILayout.Space(); //adds space 
        ToggleShowCollider();
        EditorGUILayout.Space();

        EditorGUILayout.HelpBox("Klicke \"Aktiviere Lagerplatzierung\" um vordefinierte Lager zu setzen.\n\n" +
            "Lager-Option toggle erlaubt es die Standardwerte vor der Platzierung für jeden Lagertyp zu verändern.\n\n" +
            "Beim auswählen eines Lager können Platzhalter über \"Platzhalter aktualisieren\"definiert und hinzugefügt werden.\n\n" +
            "Der \"Zufallsgenerator\" erlaubt es Objekte aus verschiedenen Kategorien einer Liste hinzuzufügen und das ausgewählte Lager damit zu befüllen.", MessageType.Info, true);

        EditorGUILayout.Space();
        ShowStoragesGUI();
        EditorGUILayout.Space();

        //The three different storage types
        switch (storageIndex)
        {
            //procedural shelf
            case 0:
                ShowHighbayOptionsGUI(storageAnim);
                standardShelfAnim.target = false;
                break;
            //free place storage
            case 1:
                ShowFreePlaceOptionsGUI();
                standardShelfAnim.target = false;
                break;
            //all standard shelfs
            case 2:
                standardShelfAnim.target = true;
                ShowStandardShelfs();
                break;
        }

        EditorGUILayout.Space();
        GUILayout.Label("Lager Funktionen", EditorStyles.boldLabel);
        EditorGUILayout.ObjectField("Ausgewähltes Lager", currentSelectedStorage, typeof(GameObject), true);
        EditorGUI.indentLevel++;
        EditorGUILayout.HelpBox("-Linksllick- auf das zu verändernde Lager, -Rechtsklick- in die Auswahlliste übrenehmen", MessageType.Info, false);
        EditorGUI.indentLevel--;

        CheckPlaceholder();

        EditorGUILayout.Space();
        GUILayout.Label("Meldungen", EditorStyles.boldLabel);

        //Error reporting
        if (setShelfMode)
        {
            EditorGUILayout.HelpBox("Regalplatzierung aktiviert!", MessageType.Info);
        }
        if (choosenRndObjects.Count == 0)
        {
            EditorGUILayout.HelpBox("Zufallsobjektliste existiert nicht. Erstellen Sie eine", MessageType.Warning);
        }
        if (currentSelectedStorage == null)
        {
            EditorGUILayout.HelpBox("Es ist momentan kein Lager zum Befüllen ausgewählt. Bitte eines auswählen", MessageType.Error);
        }
        GUILayout.EndScrollView();
    }

    private void GetActiveObject()
    {
        if (Selection.activeTransform != null)
            currentSelectedStorage = Selection.activeTransform.gameObject;
    }

    //Toggle ALL Showcollider for performance and visibility
    private void ToggleShowCollider()
    {
        EditorGUI.BeginDisabledGroup(parentStorage == null);

        GUIStyle style = new GUIStyle(GUI.skin.button);
        style.normal.textColor = selectionColor;
        if (GUILayout.Button("Toggle Gelbe Collider Ansicht", style) && parentStorage != null)
        {
            //create list from all children with ShowCollider component 
            List<ShowCollider> showColliderList = new List<ShowCollider>();
            showColliderList.AddRange(parentStorage.GetComponentsInChildren<ShowCollider>());

            foreach (ShowCollider sc in showColliderList)
            {
                sc.activateShowCollider = toggleShowCol;
            }

            //switch bool
            toggleShowCol = !toggleShowCol;
        }
        EditorGUI.EndDisabledGroup();
    }

    //GRidPreview
    private void ShowStoragesGUI()
    {
        setShelfMode = GUILayout.Toggle(setShelfMode, "Aktiviere Lagerplatzierung", "Button", GUILayout.Height(20f));

        //Get a list of previews, one for each of our prefabs
        List<GUIContent> paletteIcons = new List<GUIContent>();
        foreach (GameObject prefab in storageobjects)
        {
            //Get a preview for the prefab
            Texture2D texture = AssetPreview.GetAssetPreview(prefab);
            paletteIcons.Add(new GUIContent(texture));
        }

        //Display the grid
        storageIndex = GUILayout.SelectionGrid(storageIndex, paletteIcons.ToArray(), 3);
    }

    //Basic shelf with mesh
    private void ShowShelfOptionsGUI()
    {
        //true false for anim bool type, toggle on the left side
        storageAnim.target = EditorGUILayout.ToggleLeft("Regal Optionen", storageAnim.target);

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(storageAnim.faded))
        {
            EditorGUI.indentLevel++;
            //TODO: Eine Oberklasse/Interface mit allen values und ensptrechende Instanzen herausziehen für die benötigten Optionswerte
            //passing in customized 
            shelfColumnCount = EditorGUILayout.IntField("Anzahl Ebenen", shelfColumnCount);
            shelfRowCount = EditorGUILayout.IntField("Anzahl Plätze pro Reihe", shelfRowCount);
            shelfOffset = EditorGUILayout.Vector3Field("Versatz", shelfOffset);
            shelfPlaceholderScaling = EditorGUILayout.Vector3Field("Platzhalterskalierung", shelfPlaceholderScaling);

            EditorGUI.indentLevel--;
        }
        //sets everything back to default if its unchecked
        else
        {
            //When toggle gets unchecked, revert to default vaules.
            shelfColumnCount = 5;
            shelfRowCount = 14;
            shelfOffset = new Vector3(-1, 0, 0);
            shelfPlaceholderScaling = Vector3.one;
        }
        EditorGUILayout.EndFadeGroup();
    }

    //allow different types of fade Animation for different sections
    private void ShowHighbayOptionsGUI(AnimBool hbAnim)
    {
        //true false for anim bool type, toggle on the left side
        hbAnim.target = true;

        //fadegroup goes from 0 to 1 for slide open effect
        if (EditorGUILayout.BeginFadeGroup(hbAnim.faded))
        {
            EditorGUI.indentLevel++;
            hbHeight = EditorGUILayout.FloatField("Regalhöhe", hbHeight);
            hbColumnCount = EditorGUILayout.IntField("Anzahl Ebenen", hbColumnCount);
            hbRowCount = EditorGUILayout.IntField("Anzahl Plätze pro Reihe", hbRowCount);

            hbUseMeshAnchors = EditorGUILayout.ToggleLeft("Beachte Regalstangen für Platzhalter", hbUseMeshAnchors);

            //Additional Fadebool for advanced Options
            ShowHighbayAdvandedOptionsGUI();

            EditorGUI.indentLevel--;
        }
        //sets everything back to default if its unchecked
        else
        {
            //When toggle gets unchecked, revert to default vaules.
            hbHeight = 8;
            hbColumnCount = 4;
            hbRowCount = 8;
            hbUseMeshAnchors = true;
            advancedAnim = new AnimBool(false);
            //repaint or else stuttering
            advancedAnim.valueChanged.AddListener(Repaint);
        }
        EditorGUILayout.EndFadeGroup();
    }

    //procedural generated mesh
    private void ShowHighbayAdvandedOptionsGUI()
    {
        //true false for anim bool type, toggle on the left side
        advancedAnim.target = EditorGUILayout.ToggleLeft("Erweiterte Optionen", advancedAnim.target);

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(advancedAnim.faded))
        {
            //Preload default values
            hbHighbayMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBMetal.mat", typeof(Material));
            hbBarMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBRed.mat", typeof(Material));
            hbCrossMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBOrange.mat", typeof(Material));

            EditorGUI.indentLevel++;
            //passing in input values
            hbBetweenBarCount = EditorGUILayout.IntField("Anzahl Querstangen", hbBetweenBarCount);
            hbPoleThickness = EditorGUILayout.FloatField("Dicke Pfahlstange", hbPoleThickness);
            hbBarThickness = EditorGUILayout.FloatField("Dicke Ebenenstangen", hbBarThickness);
            hbBarHeight = EditorGUILayout.FloatField("Höhe ebenenstangen", hbBarThickness);
            hbPlaceholderScaling = EditorGUILayout.Vector3Field("Platzhalterskalierung", hbPlaceholderScaling);
            EditorGUI.BeginDisabledGroup(true);
            hbHighbayMaterial = (Material)EditorGUILayout.ObjectField("Vertikale Stangen", hbHighbayMaterial, typeof(Material), true);
            hbBarMaterial = (Material)EditorGUILayout.ObjectField("Längliche Stangen", hbBarMaterial, typeof(Material), true);
            hbCrossMaterial = (Material)EditorGUILayout.ObjectField("Quer Stangen", hbCrossMaterial, typeof(Material), true);
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.HelpBox("TODO: Materialien Instanziieren und mit Farbpalettetool anpassen (Objectfield lässt sowieso keine Auswahl zu)", MessageType.None, false);

            EditorGUI.indentLevel--;
        }
        //sets everything back to default if its unchecked
        else
        {
            //When toggle gets unchecked, revert to default vaules.
            hbBetweenBarCount = 2;
            hbPoleThickness = 0.4f;
            hbBarThickness = 0.2f;
            hbBarHeight = 0.2f;
            hbPlaceholderScaling = Vector3.one;
            hbHighbayMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBMetal.mat", typeof(Material));
            hbBarMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBRed.mat", typeof(Material));
            hbCrossMaterial = (Material)AssetDatabase.LoadAssetAtPath(path + "/Materials/HBOrange.mat", typeof(Material));
        }
        EditorGUILayout.EndFadeGroup();
    }

    //additional options before creating procedural mesh
    private void ShowFreePlaceOptionsGUI()
    {
        //true false for anim bool type, toggle on the left side
        //storageAnim.target = EditorGUILayout.ToggleLeft("Freiplatz Optionen (Holzlager)", storageAnim.target);

        if (storageIndex == 1)
            storageAnim.target = true;

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(storageAnim.faded))
        {
            EditorGUI.indentLevel++;
            //passing in customized 
            placeColumnCount = EditorGUILayout.IntField("Anzahl Ebenen", placeColumnCount);
            placeRowCount = EditorGUILayout.IntField("Anzahl Plätze pro Reihe", placeRowCount);
            placeOffset = EditorGUILayout.Vector3Field("Versatz", placeOffset);
            placePlaceholderScaling = EditorGUILayout.Vector3Field("Platzhalterskalierung", placePlaceholderScaling);
            placeholderWorldUP = EditorGUILayout.ToggleLeft("Objekte zeigen unabhängig der Regalrotation immer nach oben", placeholderWorldUP);

            EditorGUI.indentLevel--;
        }
        //sets everything back to default if its unchecked
        else
        {
            storageAnim.target = false;
            //when toggle gets unchecked, revert to default vaules.
            placeColumnCount = 4;
            placeRowCount = 8;
            placeOffset = Vector3.zero;
            placePlaceholderScaling = new Vector3(0.8f, 0.8f, 1);
            placeholderWorldUP = true;

        }
        EditorGUILayout.EndFadeGroup();
    }

    //empty placeholder spaces for spawning objects
    private void PlaceholderOptions(PlaceholderGenerator _phGenerator)
    {
        //still show disabled button even if it cant be used
        phOptionsAnim.target = EditorGUILayout.ToggleLeft("Platzhalter neu anordnen", phOptionsAnim.target);
        EditorGUI.indentLevel++;

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(phOptionsAnim.faded))
        {
            EditorGUI.indentLevel++;

            //dont update values after the fade finished
            if (phOptionsAnim.faded <= 0.9f)
            {
                //TODO: KLasse/interface hierfür
                //hand over values from the current selected storage
                optColumnCount = _phGenerator.columnCount;
                optRowCount = _phGenerator.rowCount;
                optOffset = _phGenerator.offset;
                optPlaceholderScaling = _phGenerator.placeholderScaling;
                optUseMeshAnchors = _phGenerator.useMeshAnchors;
                optholderWorldUP = _phGenerator.placeholderWorldUP;
            }

            optColumnCount = EditorGUILayout.IntField("Anzahl Ebenen", optColumnCount);
            optRowCount = EditorGUILayout.IntField("Anzahl Plätze pro Reihe", optRowCount);
            optOffset = EditorGUILayout.Vector3Field("Versatz", optOffset);
            optPlaceholderScaling = EditorGUILayout.Vector3Field("Platzhalterskalierung", optPlaceholderScaling);
            optUseMeshAnchors = EditorGUILayout.ToggleLeft("Beachte Regalstangen für Platzhalter", optUseMeshAnchors);
            optholderWorldUP = EditorGUILayout.ToggleLeft("Objekte zeigen unabhängig des Regals immer nach oben", optholderWorldUP);

            if (GUILayout.Button("Lösche Lagerplätze"))
            {
                //TODO: Mehr falsche input values abfangen
                if (optColumnCount != 0 && optRowCount != 0)
                    _phGenerator.PlaceholderParameterSetterGUI(optColumnCount, optRowCount, optOffset, optPlaceholderScaling, optUseMeshAnchors, optholderWorldUP);

                _phGenerator.CreateStorages();
                _phGenerator.isRNDFilled = false;
            }

            EditorGUI.indentLevel--;
        }
        else
        {
            //TODO: catch incorrect input values
        }
        EditorGUILayout.EndFadeGroup();
    }

    //randomize fill the placeholders with objects from the choosenRndObjects
    private void RandomFillOptions(PlaceholderGenerator _phGenerator)
    {
        GameObject selectedObject;
        randomizerAnim.target = EditorGUILayout.ToggleLeft("Zufallsgenerator für Platzhalterbefüllung", randomizerAnim.target);

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(randomizerAnim.faded))
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Befülle Prozent der Platzhalter:");
            spawnPercentage = EditorGUILayout.IntField("", Mathf.Clamp(spawnPercentage, 0, 100), GUILayout.MaxWidth(50));
            EditorGUILayout.LabelField("%");
            //EditorGUIUtility.labelWidth = 250;
            EditorGUILayout.EndHorizontal();

            //anything inside horizontal line stays the same max window width
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Min Limit Objekte: " + minQuantitySpawnLimit);
            //slider with range
            //by changing the slider, the values have to be changed to -> ref
            EditorGUILayout.MinMaxSlider(ref minQuantitySpawnValue, ref maxQuantitySpawnValue, minQuantitySpawnLimit, maxQuantitySpawnLimit);
            minQuantitySpawnValue = Mathf.RoundToInt(minQuantitySpawnValue);
            maxQuantitySpawnValue = Mathf.RoundToInt(maxQuantitySpawnValue);
            EditorGUILayout.PrefixLabel("Max Limit Objekte: " + maxQuantitySpawnLimit);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUIStyle style1 = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.UpperRight };
            EditorGUILayout.LabelField("Von: " + minQuantitySpawnValue.ToString(), style1);
            //define right align for specific texts
            GUIStyle style2 = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.UpperLeft };
            EditorGUILayout.LabelField("Bis: " + maxQuantitySpawnValue.ToString(), style2, GUILayout.ExpandWidth(true));
            EditorGUILayout.EndHorizontal();

            randomizeRotation = EditorGUILayout.ToggleLeft("Zufällige Drehung der Objekte", randomizeRotation);

            EditorGUILayout.Space();

            GUILayout.Label("Welche Objekte sollen zur Befüllung genommen werden?");

            //TODO: only update if objects changed inside the folder, events?
            ChooseFolderObjects();

            GUILayout.BeginHorizontal();

            GUIStyle style = new GUIStyle(GUI.skin.button);

            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Füge der Objektliste hinzu", style))
            {
                //check if the index is from a previous bigger list
                if (fillObjectsIndex < fillFolderObjects.Count)
                {
                    selectedObject = fillFolderObjects[fillObjectsIndex];
                    choosenRndObjects.Add(selectedObject);
                }
            }

            //If there is nothing to delete disable the button
            EditorGUI.BeginDisabledGroup(choosenRndObjects.Count == 0);

            style.normal.textColor = Color.red;

            if (GUILayout.Button("Lösche alle Objekte", style))
            {
                choosenRndObjects.Clear();
            }

            EditorGUI.EndDisabledGroup();

            GUILayout.EndHorizontal();

            //Show list of all added objects
            foreach (GameObject go in choosenRndObjects)
            {
                EditorGUILayout.ObjectField(go.name, go, typeof(GameObject), true);
            }

            //only deactivate if rndList contains no objects and only allow random filling if the placeholder exists
            EditorGUI.BeginDisabledGroup(parentStorage == null || parentStorage.transform.childCount == 0 || choosenRndObjects.Count == 0 || _phGenerator == null);

            if (GUILayout.Button("Fülle DIESE Platzhalter"))
            {
                FillStorageRandomly(_phGenerator);
            }

            EditorGUI.EndDisabledGroup();
            EditorGUI.EndDisabledGroup();

            if (parentStorage == null || parentStorage.transform.childCount == 0)
                EditorGUILayout.HelpBox("Keine Lager vorhanden zu befüllen!", MessageType.Error, false);

            if (choosenRndObjects.Count == 0)
                EditorGUILayout.HelpBox("Bitte fülle erst die Objektliste", MessageType.None, false);

            EditorGUI.indentLevel--;
        }
        //sets everything back to default if its unchecked
        else
        {
            //TODO:
        }

        EditorGUILayout.EndFadeGroup();
    }

    //get all names of the subfolders 
    private List<string> GetAllFolderNames(string parentFolderPath)
    {
        List<string> tempFolderName = new List<string>(); //to add objects in a list
        string fullPath;
        string projectName;
        objFolderPaths = AssetDatabase.GetSubFolders(parentFolderPath);
        foreach (string name in objFolderPaths)
        {
            //trimm the name to only the last part
            fullPath = Path.GetFullPath(name).TrimEnd(Path.DirectorySeparatorChar);
            projectName = Path.GetFileName(fullPath);
            tempFolderName.Add(projectName);
        }

        return tempFolderName;
    }

    //loop through all placeholder and fill them
    private void CompleteRandomFill()
    {
        //check if randomfilling is possible
        bool checkAvailable = false;

        if (choosenRndObjects.Count == 0 || parentStorage == null || parentStorage.transform.childCount == 0)
            checkAvailable = false;
        else
            checkAvailable = true;

        EditorGUI.BeginDisabledGroup(!checkAvailable);

        if (GUILayout.Button("Fülle ALLE Platzhalter"))
        {
            //get only the children (placeholder) and not the grandchildren (boxes inside placeholder)
            IEnumerable<PlaceholderGenerator> tempPHG;
            tempPHG = parentStorage.transform.Cast<Transform>().SelectMany(t => t.GetComponents<PlaceholderGenerator>());
            List<PlaceholderGenerator> tempPHGList = tempPHG.ToList();
            //get all storages and check if they are already filled, if not fill them
            foreach (PlaceholderGenerator childPhg in tempPHGList)
            {
                //only fill if its not already filled
                if (!childPhg.isRNDFilled)
                    FillStorageRandomly(childPhg);
            }
        }
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space();

        EditorGUI.BeginDisabledGroup(parentStorage == null || parentStorage.transform.childCount == 0);

        if (GUILayout.Button("Lösche ALLE Platzhalter"))
        {
            //get only the children (placeholder) and not the grandchildren (boxes inside placeholder)
            IEnumerable<PlaceholderGenerator> tempPHG;
            tempPHG = parentStorage.transform.Cast<Transform>().SelectMany(t => t.GetComponents<PlaceholderGenerator>());
            List<PlaceholderGenerator> tempPHGList = tempPHG.ToList();
            //get all storages and check if they are already filled, if so delete them
            foreach (PlaceholderGenerator childPhg in tempPHGList)
            {
                //only fill if its already filled
                if (childPhg.isRNDFilled)
                    ResetPlaceholder(childPhg);
            }
        }

        EditorGUI.EndDisabledGroup();
    }

    private void ResetPlaceholder(PlaceholderGenerator _phGenerator)
    {
        //if (optColumnCount != 0 && optRowCount != 0)
        //    _phGenerator.PlaceholderParameterSetterGUI(optColumnCount, optRowCount, optOffset, optPlaceholderScaling, optUseMeshAnchors, optholderWorldUP);

        _phGenerator.CreateStorages();
        _phGenerator.isRNDFilled = false;

    }

    //check the list inside "path" for all folders and fill the grid with their prefabs
    private void ChooseFolderObjects()
    {
        ////refresh gridview
        //true false for anim bool type, toggle on the left side
        folderObjectAnim.target = true;
        folderObjectAnim.valueChanged.AddListener(Repaint);
        //Get the right folder from the the selected type index. 
        currentFolderName = objFolderPaths[selectedObjectsTypes];
        RefreshGridPalette(); // Refresh the palette (can be called uselessly, but there is no overhead)
        selectedObjectsTypes = EditorGUILayout.IntPopup("Objekttypen: ", selectedObjectsTypes, fillFolderNames, objectTypes);

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(folderObjectAnim.faded))
        {
            EditorGUI.indentLevel++;
            //GRidPreview
            //Get a list of previews, one for each of our prefabs
            List<GUIContent> paletteIcons = new List<GUIContent>();
            Texture2D texture;
            foreach (GameObject prefab in fillFolderObjects)
            {
                //Get a preview for the prefab
                texture = AssetPreview.GetAssetPreview(prefab);
                paletteIcons.Add(new GUIContent(texture));
            }

            //Display the grid
            fillObjectsIndex = GUILayout.SelectionGrid(fillObjectsIndex, paletteIcons.ToArray(), 3);

            EditorGUILayout.Space();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFadeGroup();
    }

    private void ShowStandardShelfs()
    {
        standardShelfAnim.valueChanged.AddListener(Repaint);

        //fadegroup goes from 0 to 1 opened
        if (EditorGUILayout.BeginFadeGroup(standardShelfAnim.faded))
        {
            EditorGUI.indentLevel++;
            //GRidPreview
            //Get a list of previews, one for each of our prefabs
            List<GUIContent> paletteIcons = new List<GUIContent>();
            Texture2D texture;
            foreach (GameObject prefab in standardShelfs)
            {
                //Get a preview for the prefab
                texture = AssetPreview.GetAssetPreview(prefab);
                paletteIcons.Add(new GUIContent(texture));
            }

            //Display the grid
            standardShelfIndex = GUILayout.SelectionGrid(standardShelfIndex, paletteIcons.ToArray(), 3);

            EditorGUILayout.Space();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFadeGroup();
    }

    //check if the current selected object has placeholder or is a procedural storage
    private void CheckPlaceholder()
    {

        if (phGenerator == null)
            EditorGUILayout.HelpBox("Objekt ist kein Lager!", MessageType.Error, false);

        RandomFillOptions(phGenerator);

        EditorGUILayout.Space();

        CompleteRandomFill();
        EditorGUI.BeginDisabledGroup(phGenerator == null);
        if (GUILayout.Button("Aktualisiere/Lösche Lagerbereiche"))
        {
            ResetPlaceholder(phGenerator);
        }
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space();
        //only activate the toggle if the object has a way to generate placeholder and is a free place storage
        EditorGUI.BeginDisabledGroup(phGenerator == null || !phGenerator.placeholderWorldUP);

        GUILayout.Label("Spezial Optionen", EditorStyles.boldLabel);

        PlaceholderOptions(phGenerator);

        EditorGUI.EndDisabledGroup();

        //Show warning if the procedural shelf was created without placeholder
        if (phGenerator != null && !phGenerator.phIsGenerated)
            EditorGUILayout.HelpBox("Bitte erst Platzhalter generieren!", MessageType.Error, false);
    }

    private void LayerInfo()
    {
        if (e.type == EventType.MouseDown && e.button == 1)
        {
            if (GetPlaceholderObject() != null)
            {
                currentSelectedStorage = GetPlaceholderObject();
                Selection.activeGameObject = currentSelectedStorage;
            }
        }

        //dont intercept rightclick in the same frame, better view while dragview
        if (currentSelectedStorage != null && e.button != 1)
        {
            //if selected object is a shelf
            if (currentSelectedStorage.layer >= layerShelf && currentSelectedStorage.layer <= layerHU)
            {
                //TODO: Improve code
                //always get the shelf
                GameObject shelfObject = currentSelectedStorage;
                if (currentSelectedStorage.layer != layerShelf)
                {
                    //if its a placeholder get the parent position
                    if (currentSelectedStorage.layer == layerPlaceholder)
                    {
                        currentSelectedLayer = layerPlaceholder;
                        shelfObject = currentSelectedStorage.transform.parent.transform.gameObject;
                        currentSelectedStorage.GetComponent<ShowCollider>().colliderColor = selectionColor;
                    }

                    //if it is a HU get the parent parent position
                    if (currentSelectedStorage.layer == layerHU)
                    {
                        currentSelectedLayer = layerHU;
                        shelfObject = currentSelectedStorage.transform.parent.transform.parent.transform.gameObject;
                    }
                }
                else
                    currentSelectedLayer = layerShelf;

                phGenerator = shelfObject.GetComponent<PlaceholderGenerator>();

                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.black;
                style.fontStyle = FontStyle.Bold;
                style.normal.background = BGColor(600, 1, new Color(1.0f, 1.0f, 1.0f, 0.3f));
                //style.normal.background = new Texture2D(600, 1);

                //Textinfo for selected storage
                Handles.color = Color.blue;
                Handles.Label(shelfObject.transform.position + Vector3.up * 5 + Vector3.right,
                    "L: " + String.Format("{0:0.00}", shelfObject.GetComponent<BoxCollider>().size.x) + " B: " + String.Format("{0:0.00}", shelfObject.GetComponent<BoxCollider>().size.z) + " H: " + String.Format("{0:0.00}", shelfObject.GetComponent<BoxCollider>().size.y) +
                    "\nPlätze: " + shelfObject.transform.childCount, style);

                Handles.BeginGUI();
                //Set the Camera to shelf position
                if (GUILayout.Button(shelfObject.name, GUILayout.Width(150)))
                {
                    Vector3 scenePosition = SceneView.lastActiveSceneView.pivot;
                    scenePosition = new Vector3(shelfObject.transform.position.x, scenePosition.y, shelfObject.transform.position.z);
                    SceneView.lastActiveSceneView.pivot = scenePosition;
                    SceneView.lastActiveSceneView.Repaint();
                }
                Handles.EndGUI();

                Handles.DrawWireArc(shelfObject.transform.position,
                    shelfObject.transform.up,
                    shelfObject.transform.right,
                    180,
                    5);

                //    Handles.ScaleValueHandle(5,
                //        handleExample.transform.position + handleExample.transform.forward * 5,
                //        handleExample.transform.rotation,
                //        1,
                //        Handles.ConeHandleCap,
                //        1);

            }
            else
                currentSelectedLayer = 0;

            //if selected object is a placeholder
            if (currentSelectedLayer == layerPlaceholder)
            {
                storageScript = currentSelectedStorage.GetComponent<StorageScript>();
                Handles.BeginGUI();
                string phInfo = "Name: " + currentSelectedStorage.name + "\nGefüllt: " + storageScript.GetIsFilled() + "\nAnzahl: " + storageScript.storageList.Count;
                //EditorGUILayout.LabelField(huInfo, GUILayout.Width(EditorGUIUtility.labelWidth - 4));
                //EditorGUILayout.HelpBox(huInfo, MessageType.None, false);
                GUI.Label(new Rect(4, 80, 150, 40), phInfo, "TextField");

                EditorGUI.BeginDisabledGroup(choosenRndObjects.Count == 0);

                //add a button to spawn a object inside the placeholder
                if (GUILayout.Button("Objekt Hinzufügen", GUILayout.Width(150)))
                {
                    GameObject spawnObject = null;

                    //if the placeholder is empty and a object is selected, take the first selected
                    if (storageScript.storageList.Count == 0)
                    {
                        spawnObject = choosenRndObjects.First();
                    }
                    //else search for every object inside fillobject folder and find the matching type object by comparing their meshnames
                    else if (storageScript.storageList.Count != 0)
                    {
                        spawnObject = storageScript.storageList.First();

                        GameObject compareObj;
                        //every folder in fillObjects
                        for (int i = 0; i < fillFolderNames.Count(); i++)
                        {
                            string[] folderFiles = System.IO.Directory.GetFiles(path + "/" + subPath + "/" + fillFolderNames[i], "*.prefab");

                            //every prefab in type folder
                            for (int j = 0; j < folderFiles.Count(); j++)
                            {
                                compareObj = AssetDatabase.LoadAssetAtPath(folderFiles[j], typeof(GameObject)) as GameObject;

                                //compare the names without numbers
                                if (Regex.Replace(spawnObject.name, @"(\p{L}+).*", "$1").Equals(Regex.Replace(compareObj.name, @"(\p{L}+).*", "$1")))
                                {
                                    spawnObject = compareObj;
                                    break;
                                }
                            }
                        }
                    }
                    storageScript.SpawnOverlapless(spawnObject);
                }

                EditorGUI.EndDisabledGroup();

                EditorGUI.BeginDisabledGroup(storageScript.storageList.Count == 0);

                //add a button to spawn a object inside the placeholder
                if (GUILayout.Button("Lösche Objekt", GUILayout.Width(150)))
                {
                    storageScript.DeleteLIFO();
                }

                EditorGUI.EndDisabledGroup();
                Handles.EndGUI();
            }

            //if selected object is a handling unit
            if (currentSelectedLayer == layerHU)
            {
                Handles.BeginGUI();
                string huInfo = "HuNum: " + currentSelectedStorage.name + "\nPriorität: " + 2 + "\nInhalt: " + 3;
                //EditorGUILayout.LabelField(huInfo, GUILayout.Width(EditorGUIUtility.labelWidth - 4));
                //EditorGUILayout.HelpBox(huInfo, MessageType.None, false);
                GUI.Label(new Rect(4, 40, 150, 40), huInfo, "TextField");
                Handles.EndGUI();
            }
        }
    }

    //Returns the four corners of the drag box
    private Vector3[] GetDragCorners()
    {
        //HandleUtility.Repaint();
        Vector3 mPos = GetMouseWorldPos();

        //bottom left start, rectangle goes counter clockwise
        Vector3[] corners = new Vector3[]
        {
            dragStartPos,
            new Vector3(mPos.x, dragStartPos.y, dragStartPos.z),
            new Vector3(mPos.x, dragStartPos.y, mPos.z),
            new Vector3(dragStartPos.x, dragStartPos.y, mPos.z)
        };

        Handles.DrawSolidRectangleWithOutline(corners, new Color(0.9f, 0, 0, 0.1f), new Color(0, 0, 0, 1));

        foreach (Vector3 posCube in corners)
        {
            Handles.ScaleValueHandle(5,
                    posCube,
                    Quaternion.identity,
                    1.0f,
                    Handles.CubeHandleCap,
                    1.0f);
        }

        middle = CalculateMiddlePoint(dragStartPos, corners);

        //Draw the arrow handler in the middle of the correct line
        if (direction != Vector3.zero)
        {
            ShowArrowHandling(middle, direction);
            //x and z will later be correctly converted to the shelfs length and width
            Handles.Label(mPos + new Vector3(-2, 0, -2), "x: " + String.Format("{0:0.00}", corners[1].x));
            Handles.Label(mPos + new Vector3(-2, 0, 0), "z: " + String.Format("{0:0.00}", corners[2].z));
        }

        return corners;
    }

    private Vector3 GetMouseWorldPos()
    {
        RaycastHit hit;
        if (Physics.Raycast(HandleUtility.GUIPointToWorldRay(e.mousePosition), out hit))
        {
            return hit.point;
        }
        return Vector3.zero;
    }

    //returns a hit object from layer "placeholder"
    private GameObject GetPlaceholderObject()
    {
        RaycastHit hit;
        GameObject hitObj = null;

        //shift the int layer to bit layermask
        if (Physics.Raycast(HandleUtility.GUIPointToWorldRay(e.mousePosition), out hit, 200, 1 << layerPlaceholder))
        {
            if (hit.transform != null)
            {
                //reset current placeholder color before next one gets selected
                if (currentSelectedLayer == layerPlaceholder)
                    RevertSelectionColor();

                hitObj = hit.transform.gameObject;
            }
        }
        return hitObj;
    }

    //get prefabs if the folder changed
    private void RefreshGridPalette()
    {
        //clear list to not add to current list
        storageobjects.Clear();
        fillFolderObjects.Clear();
        standardShelfs.Clear();

        //StorageGrid
        string[] storageFiles = System.IO.Directory.GetFiles(path + "/Storages", "*.prefab");
        foreach (string storageFile in storageFiles)
            storageobjects.Add(AssetDatabase.LoadAssetAtPath(storageFile, typeof(GameObject)) as GameObject);

        //StandardShelfGrid
        string[] standardShelfsFiles = System.IO.Directory.GetFiles(path + "/Storages/StandardShelfs", "*.prefab");
        foreach (string standardFile in standardShelfsFiles)
            standardShelfs.Add(AssetDatabase.LoadAssetAtPath(standardFile, typeof(GameObject)) as GameObject);

        //FillobjectGrid
        if (!String.IsNullOrEmpty(currentFolderName))
        {
            string[] folderFiles = System.IO.Directory.GetFiles(currentFolderName, "*.prefab");
            foreach (string folderFile in folderFiles)
                fillFolderObjects.Add(AssetDatabase.LoadAssetAtPath(folderFile, typeof(GameObject)) as GameObject);
        }
    }

    private void ShelfModes()
    {
        //disable Unity select for mouseup to work
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        mouseCenter = GetMouseWorldPos();
        switch (storageIndex)
        {
            //Only activate drag select when procedural shelf is selected
            case 0:
                //resett start position with right click
                if (e.type == EventType.MouseDown && e.button == 0 || e.control)
                {
                    dragging = true;
                    dragStartPos = GetMouseWorldPos();
                }

                //spawn only works if drag corners dont cast rays
                if (e.type == EventType.MouseUp && e.button == 0 && corners != null && dragging)
                    SpawnObjectsInScene(mouseCenter, storageobjects, storageIndex, corners);
                else if (dragging)
                    corners = GetDragCorners();
                break;
            case 1:
                dragging = false;
                SpawnObjectsInScene(mouseCenter, storageobjects, storageIndex);
                break;
            default:
                dragging = false;
                SpawnObjectsInScene(mouseCenter, standardShelfs, standardShelfIndex);
                break;
        }
    }

    private Texture2D BGColor(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    //spawn selected grid object at current mouse position
    private void SpawnObjectsInScene(Vector3 mousePos, List<GameObject> objectList, int objectIndex, Vector3[] corners = null)
    {
        //We have a prefab selected and we are clicking in the scene view with the left button
        if (objectIndex < objectList.Count && e.type == EventType.MouseUp && e.button == 0 || e.shift)
        {
            //Create the prefab instance while keeping the prefab link
            GameObject prefab = objectList[objectIndex];

            GameObject go = Instantiate(prefab) as GameObject;
            //PrefabUtility.UnpackPrefabInstance(go, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
            go.transform.name += " " + incrementID;
            incrementID++;

            //Set all new storages as child of a parent
            GameObject parent = GameObject.Find(parentString) ?? new GameObject(parentString);
            go.transform.SetParent(parent.transform);

            StorageGenerator _storeGen = go.GetComponent<StorageGenerator>();

            //creates highbay when its set, and replace go with it
            if (_storeGen != null)
            {
                SetValues(_storeGen.transform.gameObject, 4);
                _storeGen.CreateShelf(out go, corners[0], -corners[1], corners[2], rotation, hbID);
                hbID++;
                //set new Storegen to the created Shelf
                _storeGen = go.GetComponent<StorageGenerator>();
            }
            //for non procedural stoages
            else
            {
                //shift the object half a size up if it has a BoxCollider
                if (prefab.GetComponent<BoxCollider>() != null)
                    go.transform.position = mousePos + new Vector3(0, prefab.GetComponent<BoxCollider>().size.y / 2, 0);
                else
                    go.transform.position = mousePos;
            }

            //set input values to created storage and create placeholder
            //SetValues(go, storageIndex);

            //TODO: fire event from the created highbay to set it as current selected
            //set spawned object as selected
            parentStorage = parent;
            currentSelectedStorage = go;

            // Allow the use of Undo (Ctrl+Z, Ctrl+Y).
            if (go != null)
                Undo.RegisterCreatedObjectUndo(go, "");

            setShelfMode = false;
            dragging = false;

            //reactivate selection
            //HandleUtility.Repaint();
            Repaint();
        }
    }

    //Calculates so the start point of the drag is always the bottom left of the shelf
    private Vector3 CalculateMiddlePoint(Vector3 startPos, Vector3[] corners)
    {
        Vector3[] newCorners = corners;

        //always set the left shelf corner to dragStartPos
        //drag top right - counter clockwise
        if (corners[2].x > startPos.x && corners[2].z > startPos.z)
        {
            newCorners[0] = corners[0];
            middle = corners[1] + (corners[2] - corners[1]) / 2;
            direction = Vector3.right;

            newCorners[2].z = Mathf.Abs(startPos.x - corners[1].x);
            newCorners[1].x = Mathf.Abs(startPos.z - corners[3].z);
            rotation = new Vector3(0, 90, 0);
        }
        //drag top left - clockwise
        else if (corners[2].x < startPos.x && corners[2].z > startPos.z)
        {
            newCorners[0] = corners[0];
            middle = corners[2] + (corners[3] - corners[2]) / 2;
            direction = Vector3.forward;

            newCorners[1].x = Mathf.Abs(startPos.x - corners[1].x);
            newCorners[2].z = Mathf.Abs(startPos.z - corners[3].z);
            rotation = new Vector3(0, 0, 0);
        }
        //drag bottom left - counter clockwise
        else if (corners[2].x < startPos.x && corners[2].z < startPos.z)
        {
            newCorners[0] = corners[0];
            middle = corners[1] + (corners[2] - corners[1]) / 2;
            direction = -Vector3.right;

            newCorners[2].z = Mathf.Abs(startPos.x - corners[1].x);
            newCorners[1].x = Mathf.Abs(startPos.z - corners[3].z);
            rotation = new Vector3(0, 270, 0);
        }
        //drag bottom right - clockwise
        else if (corners[2].x > startPos.x && corners[2].z < startPos.z)
        {
            newCorners[0] = corners[0];
            middle = corners[3] + (corners[2] - corners[3]) / 2;
            direction = -Vector3.forward;

            newCorners[1].x = Mathf.Abs(startPos.x - corners[1].x);
            newCorners[2].z = Mathf.Abs(startPos.z - corners[3].z);
            rotation = new Vector3(0, 180, 0);
        }
        return middle;
    }

    //Draws an arrow in which direction the shelf will be facing
    private void ShowArrowHandling(Vector3 pos, Vector3 lookTo)
    {
        Handles.color = Handles.zAxisColor;
        Handles.ArrowHandleCap(
            0,
            pos,
            Quaternion.LookRotation(lookTo),
            20,
            EventType.Repaint
        );
    }

    //pass the created shelf the GUI values
    private void SetValues(GameObject go, int functionIndex)
    {
        //TODO: komplette setter mit klassen/interfaces ersetzen
        switch (functionIndex)
        {
            case 0:
                PlaceholderGenerator phHighbay = go.GetComponent<PlaceholderGenerator>();
                phHighbay.PlaceholderParameterSetterGUI(hbColumnCount, hbRowCount, Vector3.zero, hbPlaceholderScaling, hbUseMeshAnchors, false);
                break;
            case 1:
                PlaceholderGenerator phGenPlace = go.GetComponent<PlaceholderGenerator>();
                phGenPlace.PlaceholderParameterSetterGUI(placeColumnCount, placeRowCount, placeOffset, placePlaceholderScaling, false, placeholderWorldUP);
                break;
            //set StorageGenerator values
            case 4:
                StorageGenerator storeGen = go.GetComponent<StorageGenerator>();
                storeGen.StorageGeneratorSetterGUI(hbHeight, hbColumnCount, hbRowCount, hbPlaceholderScaling,
                    hbBetweenBarCount, hbPoleThickness, hbBarThickness, hbBarHeight,
                    hbHighbayMaterial, hbBarMaterial, hbCrossMaterial);
                break;
            default:
                PlaceholderGenerator phGenShelf = go.GetComponent<PlaceholderGenerator>();
                phGenShelf.PlaceholderParameterSetterGUI(shelfColumnCount, shelfRowCount, shelfOffset, shelfPlaceholderScaling, false, false);
                break;
        }
    }

    //TODO: UnityEngine.Random in einer neuen Klasse ersetzen
    //fill all placeholder from a shelf randomly
    public void FillStorageRandomly(PlaceholderGenerator goToFill)
    {
        GameObject rndObj;
        GameObject tempObj;

        for (int rw = 1; rw <= goToFill.rowCount; rw++)
        {
            for (int col = 1; col <= goToFill.columnCount; col++)
            {
                //spawnt je nach Prozentwahrscheinlichkeit etwas im Platzhalter
                //spawn with a percentage probability at the placeholder
                if (UnityEngine.Random.value * 100 < spawnPercentage)
                {
                    //int values for random numbers must be assigned on class level
                    //TODO: improve, check if it always works
                    rndNumberRange = UnityEngine.Random.Range(0, choosenRndObjects.Count); //Random Range is always below Count
                    //take randomly an object of the list
                    rndObj = choosenRndObjects[rndNumberRange];

                    //if object has tag "SingleSpawn" only spawn one of it in the placeholder
                    if (rndObj.tag == "SingleSpawn")
                    {
                        goToFill.SpawnAtShelfPos(rw, col, rndObj, false);
                    }
                    else
                    {
                        //spawn at a random number multiple objects in the placeholder
                        int rndNumberSpawn = (int)UnityEngine.Random.Range(minQuantitySpawnValue, maxQuantitySpawnValue);

                        for (int i = 0; i <= rndNumberSpawn; i++)
                        {
                            //TODO: optimize it for all kinds of objects, currently only x rotation
                            //create always a new object with new rotation or else the rotation will be called to the sameprefab multiple times
                            tempObj = Instantiate(rndObj);
                            //rotate for visual effects to look different
                            if (randomizeRotation)
                                tempObj.transform.rotation *= Quaternion.Euler(new Vector3(UnityEngine.Random.Range(0, 270), 0, 0));

                            goToFill.SpawnAtShelfPos(rw, col, tempObj, false);
                            DestroyImmediate(tempObj);
                        }
                    }
                }
            }
        }
        goToFill.isRNDFilled = true;
    }

        //if current selection was a placeholder, revert its collider color to its standard
    private void RevertSelectionColor()
    {
        if (currentSelectedStorage.GetComponent<ShowCollider>() != null)
            currentSelectedStorage.GetComponent<ShowCollider>().colliderColor = Color.yellow;
    }

    //returns selected objects
    public List<GameObject> GetRandomObjectList()
    {
        return choosenRndObjects;
    }


}