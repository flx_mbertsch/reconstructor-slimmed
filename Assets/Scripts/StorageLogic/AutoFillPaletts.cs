﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Testclass to fill specific shelfs
public class AutoFillPaletts : MonoBehaviour
{
    [Header("SAP Infos")]
    public string Maske;
    public string placeholderNaming;

    [Header("RND Parameter")]
    public int spawnInPercentOfBoxes;
    [Range(1, 50)]
    public int spawnMaxBoxes;
    public int spawnWoodPercent;
    [Range(1, 20)]
    public int SpawnMaxWoods;
    public GameObject boxes;
    public GameObject chair;
    public List<GameObject> woods;

    [Header("Storages")]
    public GameObject shelf;
    public GameObject TestStorage;
    public GameObject woodPlace;

    public Vector2 placeholderPosition;

    public Vector3 storageSize;
    public List<Vector3> shelfCornerPos = new List<Vector3>();

    private float percentageFill;
    PlaceholderGenerator shelfSpawn1;
    PlaceholderGenerator woodPlaceSpawn;

    // Start is called before the first frame update
    void Start()
    {
        shelfSpawn1 = shelf.GetComponentInChildren<PlaceholderGenerator>();
        woodPlaceSpawn = woodPlace.GetComponentInChildren<PlaceholderGenerator>();
        StartCoroutine(StartTA2());
        StartCoroutine(StartTA3());
    }

    //Only for Testing
    IEnumerator StartTA2()
    {
        yield return new WaitForSeconds(1);
        int randomBoxCount = 1;

        for (int rw = 1; rw <= shelfSpawn1.rowCount; rw++)
        {
            for (int col = 1; col <= shelfSpawn1.columnCount; col++)
            {
                //spawnt je nach Prozentwahrscheinlichkeit eine Box
                if (Random.value * 100 < spawnInPercentOfBoxes)
                {
                    //spawnt zufällig mehrer Boxen im Regal
                    randomBoxCount = (int)(Random.value * spawnMaxBoxes);
                    for (int i = 0; i <= randomBoxCount; i++)
                    {
                        SpawnPallet(shelf, rw, col, boxes, false);
                    }
                }
            }
        }
        shelfSpawn1.isRNDFilled = true;
    }

    //Only for Testing
    IEnumerator StartTA3()
    {
        yield return new WaitForSeconds(1);

        int randomTreeCount = 1;
        int randomWoodObject = 3;

        for (int rw = 1; rw <= woodPlaceSpawn.rowCount; rw++)
        {
            for (int col = 1; col <= woodPlaceSpawn.columnCount; col++)
            {
                //spawnt je nach Prozentwahrscheinlichkeit etwas im Platzhalter
                if (Random.value * 100 < spawnWoodPercent)
                {
                    randomWoodObject = Random.Range(0, 5);

                    //falls es ein Einzelobjekt ist, spawn nur eins im Platzhalter
                    if (woods[randomWoodObject].tag == "SingleSpawn")
                    {
                        SpawnPallet(woodPlace, rw, col, woods[randomWoodObject], true);
                    }
                    else
                    {
                        //spawnt zufällig mehrer Bäume im Regal
                        randomTreeCount = (int)(Random.value * SpawnMaxWoods);

                        for (int i = 0; i <= randomTreeCount; i++)
                        {
                            //TODO: Optimieren
                            //Damit die Rotation nicht ständig auf das selbe prefab angewandt wird muss immer ein neues erstellt werden
                            GameObject temp = Instantiate(woods[randomWoodObject]);
                            //Rotiert Objekt immer zufällig um die xAchse, damit es immer anders aussieht
                            temp.transform.rotation *= Quaternion.Euler(new Vector3(Random.Range(0,270),0,0));
                            SpawnPallet(woodPlace, rw, col, temp, false);
                            DestroyImmediate(temp);
                        }

                    }
                }
            }
        }
        woodPlaceSpawn.isRNDFilled = true;
    }

    public void SpawnPallet(GameObject shelfToSpawn, int rowSpawn, int columnSpawn, GameObject spawnObject, bool spawnSingle)
    {
        //Überprüft ob der Platz schon befüllt ist und ob es eine Placeholder Klasse hat
        if (shelfToSpawn != null && shelfToSpawn.activeSelf && shelfToSpawn.GetComponentInChildren<PlaceholderGenerator>() != null)
            shelfToSpawn.GetComponentInChildren<PlaceholderGenerator>().SpawnAtShelfPos(rowSpawn, columnSpawn, spawnObject, spawnSingle);
        else
            Debug.Log("Objekt besitzt keine PlaceholderKlasse");
    }

    public void GetPercentageFillShelf()
    {
        if (Application.isPlaying && shelfSpawn1 != null)
        {
            percentageFill = shelfSpawn1.BoxfilledInPercentage((int)placeholderPosition.x, (int)placeholderPosition.y);
            Debug.Log("Box at row " + placeholderPosition.x + " and Columns " + placeholderPosition.y + " is filled " + percentageFill + "%");
        }
    }

    public void AddAnotherShelfBox()
    {
        shelfSpawn1.SpawnAtShelfPos((int)placeholderPosition.x, (int)placeholderPosition.y, boxes, false);
    }

    public void DeleteShelfBox()
    {
        GameObject lastAddedBox = shelfSpawn1.GetGameObjectFromPlaceholder((int)placeholderPosition.x, (int)placeholderPosition.y, 0);
        shelfSpawn1.DeleteContent((int)placeholderPosition.x, (int)placeholderPosition.y, lastAddedBox);
    }

    public void AddOverlappless()
    {
        TestStorage.GetComponent<StorageScript>().SpawnOverlapless(boxes);
    }

    public void AddOverlapplessChair()
    {
        TestStorage.GetComponent<StorageScript>().SpawnOverlapless(chair);
    }

    public void TestOverlapDelete()
    {
        TestStorage.GetComponent<StorageScript>().DeleteLIFO();
    }

    public void CalculateCornerPoints()
    {
        shelfCornerPos = shelfSpawn1.CalculateCornerPoints();
    }

    public void CalculateStorageSize()
    {
        storageSize = Vector3.Scale(shelfSpawn1.GetComponent<BoxCollider>().size, transform.localScale);
    }

}
