﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//TODO: erben von storagegenerator

//Generates Placeholders inside the shelf places for objects to spawn inside
public class PlaceholderGenerator : MonoBehaviour
{
    [Header("User Settings")]
    public int columnCount;
    public int rowCount;
    public Vector3 offset;
    public Vector3 placeholderScaling = Vector3.one;
    public bool useMeshAnchors;
    public bool placeholderWorldUP = false;

    [Header("Placeholder Options")]
    public bool phIsGenerated = false;
    public bool isRNDFilled = false;
    public Vector3 shelfSize;
    public Vector3 placeholderSize;
    public int layerPlaceHolder = 10;
    //if usePlaceholderAnchors, than fill the list with anchors
    public List<Vector3> anchorsPHList = new List<Vector3>();

    private Vector3 objectCenter;
    private Vector3 objectStartPosition;
    private Vector3 tempObjectCorner;

    private float objectWidth;
    private float objectHeight;
    private float objectDepth;

    private BoxCollider shelfCol;
    private Vector3 bottomLeftShelf;
    private Vector3 bottomRightShelf;
    private Vector3 topLeftShelf;

    //TODO: Struct/Interface für setter&Getter definieren
    public void PlaceholderParameterSetterGUI(int columnCount, int rowCount, Vector3 offset, Vector3 placeholderScaling, bool useMeshAnchors, bool placeholderWorldUP)
    {
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        this.offset = offset;
        this.placeholderScaling = placeholderScaling;
        this.useMeshAnchors = useMeshAnchors;
        this.placeholderWorldUP = placeholderWorldUP;
    }

    //TODO: Struct/Interface für setter&Getter definieren
    public void PlaceHolderParameterSetter(int columnCount, int rowCount, Vector3 placeholderScaling, Vector3 offset, Vector3 placeholderSize, int layerPlaceHolder)
    {
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        this.placeholderScaling = placeholderScaling;
        this.offset = offset;
        this.layerPlaceHolder = layerPlaceHolder;
        this.placeholderSize = placeholderSize;
    }

    private void OnDrawGizmos()
    {
        //shelfCol = GetComponent<BoxCollider>();
        ////mitte Regal
        //Gizmos.DrawWireSphere(transform.TransformPoint(GetComponent<BoxCollider>().center), 0.4f);

        ////vorne unten links
        //Gizmos.color = Color.green;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////vorne unten rechts
        //Gizmos.color = Color.red;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(-shelfCol.size.x, -shelfCol.size.y, shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////hinten unten rechts
        //Gizmos.color = Color.yellow;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(-shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////hinten unten links
        //Gizmos.color = Color.blue;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////vorne oben links
        //Gizmos.color = Color.cyan;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, shelfCol.size.y, shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////vorne oben rechts
        //Gizmos.color = Color.red;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(-shelfCol.size.x, shelfCol.size.y, shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////hinten oben rechts
        //Gizmos.color = Color.yellow;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(-shelfCol.size.x, shelfCol.size.y, -shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));
        ////hinten oben links
        //Gizmos.color = Color.blue;
        //Gizmos.DrawCube(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, shelfCol.size.y, -shelfCol.size.z) * 0.5f), new Vector3(0.4f, 0.4f, 0.4f));

        //Vector3 tempLeft = transform.TransformPoint(bottomLeftShelf);
        //Vector3 tempRight = transform.TransformPoint(bottomRightShelf);
        //Vector3 tempTop = transform.TransformPoint(topLeftShelf);
        //Gizmos.color = Color.magenta;
        ////Zeichne horizontale Startlinie
        //Gizmos.DrawLine(tempLeft, tempRight);
        //Gizmos.DrawWireSphere(GetPointDistanceFromObject(-Vector3.Distance(tempLeft, tempRight) / rowCount, tempLeft, tempRight), 0.4f);

        ////Zeichne vertikale Startlinie
        //Gizmos.DrawLine(tempLeft, tempTop);
        //Gizmos.DrawWireSphere(GetPointDistanceFromObject(-Vector3.Distance(tempLeft, tempTop) / columnCount, tempLeft, tempTop), 0.4f);
    }

    // Use this for initialization
    //void Start()
    //{
    //    CreateStorages();
    //}

    //For info purposes only
    private void Update()
    {
        //Gets the Size of current Object
        objectWidth = GetComponentInParent<BoxCollider>().size.x;
        objectHeight = GetComponentInParent<BoxCollider>().size.y;
        objectDepth = GetComponentInParent<BoxCollider>().size.z;

        GetShelfSize();
    }

    //Sets the placeholder inside the shelf
    public void CreateStorages()
    {
        //TODO: if funktioniert nicht daher while
        //check if object already has children, if so, delete them all before filling

        while (transform.childCount > 0)
        {
            foreach (Transform child in transform)
            {
                DestroyImmediate(child.gameObject);
            }
        }

        shelfCol = GetComponent<BoxCollider>();
        objectCenter = shelfCol.center;

        bottomLeftShelf = shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f;
        bottomRightShelf = shelfCol.center + new Vector3(-shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f;
        topLeftShelf = shelfCol.center + new Vector3(shelfCol.size.x, shelfCol.size.y, -shelfCol.size.z) * 0.5f;

        if (!useMeshAnchors)
            CreatePlaceholderAnchors();
        else
        {
            Vector3 ScaledPlaceholderSize = Vector3.Scale(placeholderSize, transform.lossyScale);
            SpawnStorageAtAnchors(anchorsPHList, ScaledPlaceholderSize);
        }
    }

    //Creates Placeholder at the given position with the given size
    public GameObject CreateBasicPlaceholder(Vector3 position, Vector3 placeholderSize)
    {
        GameObject storageObject = new GameObject("Unlabeled Storage");
        storageObject.transform.position = position;
        storageObject.transform.rotation = transform.rotation;
        //set the scaling of the placeholder the same as the scaling of the Shelf
        storageObject.transform.localScale = placeholderSize;
        BoxCollider bCol = storageObject.AddComponent<BoxCollider>();
        bCol.isTrigger = true;
        storageObject.AddComponent<ShowCollider>();
        storageObject.AddComponent<StorageScript>().CalculateCorner();
        storageObject.layer = layerPlaceHolder;

        //Allow the use of Undo (Ctrl+Z, Ctrl+Y).
        Undo.RegisterCreatedObjectUndo(storageObject, "");

        return storageObject;
    }

    //Creates anchorpoints for placeholder by calculating the size of the BoxCollider and seperating each section in equal parts 
    //Calculates first the anchors and then shifts with TransformPoint to the shelf (size and rotation stable)
    private void CreatePlaceholderAnchors()
    {
        Vector3 anchorPos;
        List<Vector3> storageAnchors = new List<Vector3>();
        float xDistance;
        float yDistance;
        Vector3 posX;
        Vector3 posY;
        Vector3 scaledSize;

        //Use the centre of the object for corner point calculation
        anchorPos = objectCenter;
        anchorPos -= GetObjectAnchor() / 2;
        objectStartPosition = anchorPos;

        //offsets if the shelf is uneven
        bottomLeftShelf = transform.TransformPoint(bottomLeftShelf + offset);
        bottomLeftShelf = transform.InverseTransformPoint(bottomLeftShelf);

        //Dont add offsetX to the right bottom end
        bottomRightShelf = transform.TransformPoint(bottomRightShelf + new Vector3(0, offset.y, offset.z));
        bottomRightShelf = transform.InverseTransformPoint(bottomRightShelf);

        //Dont add offsetY at the top left end
        topLeftShelf = transform.TransformPoint(topLeftShelf + new Vector3(offset.x, 0, offset.z));
        topLeftShelf = transform.InverseTransformPoint(topLeftShelf);

        xDistance = Vector3.Distance(bottomLeftShelf, bottomRightShelf);
        yDistance = Vector3.Distance(bottomLeftShelf, topLeftShelf);

        //crate anchorpoints with offset
        for (int rw = 0; rw < rowCount; rw++)
        {
            for (int col = 0; col < columnCount; col++)
            {
                //Calculate length and height of the shelf. Separate the lenght and height by the counts of row and column
                posX = GetPointDistanceFromObject(-xDistance / rowCount * rw, bottomLeftShelf, bottomRightShelf);
                posY = GetPointDistanceFromObject(-yDistance / columnCount * col, bottomLeftShelf, topLeftShelf);
                anchorPos = new Vector3(posX.x, posY.y, posX.z);

                storageAnchors.Add(anchorPos);
            }
        }
        //if the placeholder has to be always upand be independent of the shelf rotation, switch z and y
        if (placeholderWorldUP)
        {
            placeholderSize = new Vector3((shelfCol.size.x + offset.x) / rowCount, shelfCol.size.z - offset.y, (shelfCol.size.y - offset.y) / columnCount);
            //rescale placeholder with input values
            scaledSize = Vector3.Scale(placeholderSize, new Vector3(transform.lossyScale.x, transform.lossyScale.z, transform.lossyScale.y));
            SpawnStorageAtAnchors(storageAnchors, scaledSize);
        }
        else
        {
            placeholderSize = new Vector3((shelfCol.size.x + offset.x) / rowCount, (shelfCol.size.y - offset.y) / columnCount, shelfCol.size.z);
            //rescale placeholder with input values
            scaledSize = Vector3.Scale(placeholderSize, transform.lossyScale);
            SpawnStorageAtAnchors(storageAnchors, scaledSize);
        }
    }

    List<Vector3> gizmoAnchors = new List<Vector3>();
    //set placeholders at their given anchorpoints in the shelf
    public void SpawnStorageAtAnchors(List<Vector3> anchorPoints, Vector3 placeholderSize)
    {
        gizmoAnchors = anchorPoints;
        int rwName = 0;
        int colName = 0;
        GameObject tempObject;

        placeholderSize = Vector3.Scale(placeholderSize, placeholderScaling);
        GameObject storageSpaceObject = CreateBasicPlaceholder(Vector3.zero, placeholderSize);
        //calculate the shift without the shelf size (independend from shelf scaling)
        Vector3 objLocalSize = Vector3.Scale(storageSpaceObject.GetComponent<BoxCollider>().size, storageSpaceObject.transform.localScale);
        //TODO: shift to middle extra method
        //shift the placeholder half the placeholder size to the middle
        Vector3 shiftToMiddle = new Vector3(-objLocalSize.x / 2 / transform.localScale.x, objLocalSize.y / 2 / transform.localScale.y, objLocalSize.z / 2 / transform.localScale.z);

        //spawn at every anchor the placeholder with shift
        foreach (Vector3 anchor in anchorPoints)
        {
            //Important: dont use TransformPoint before this
            tempObject = Instantiate(storageSpaceObject, transform.TransformPoint(anchor + shiftToMiddle), transform.rotation) as GameObject;
            //calculates the cornerspoints for each ph
            tempObject.GetComponent<StorageScript>().CalculateCorner();

            if (placeholderWorldUP)
            {
                tempObject.transform.rotation = Quaternion.identity;
            }
            //start names at 1 like in SAP
            tempObject.name = ("Lagerplatz " + "R" + (rwName + 1) + "C" + (colName + 1));
            tempObject.transform.SetParent(transform);
            rwName++;

            //reset the rwName after each row and increment the column with 1
            if (rwName % rowCount == 0)
            {
                colName++;
                rwName = 0;
            }

            // Allow the use of Undo (Ctrl+Z, Ctrl+Y).
            Undo.RegisterCreatedObjectUndo(tempObject, "");
        }

        phIsGenerated = true;
        //Regal placeholder werden neu instanziiert. Deshalb zerstöre das ursprüngliche Objekt
        //placeholders at anchors got newly instantiated so destroy origin placeholder
        DestroyImmediate(storageSpaceObject);
    }

    public List<Vector3> CalculateCornerPoints()
    {
        List<Vector3> cornerpoints = new List<Vector3>();
        //hinten unten links
        cornerpoints.Add(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f));
        //hinten unten rechts
        cornerpoints.Add(transform.TransformPoint(shelfCol.center + new Vector3(-shelfCol.size.x, -shelfCol.size.y, -shelfCol.size.z) * 0.5f));
        //vorne unten rechts
        cornerpoints.Add(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, shelfCol.size.z) * 0.5f));
        //vorne unten links
        cornerpoints.Add(transform.TransformPoint(shelfCol.center + new Vector3(shelfCol.size.x, -shelfCol.size.y, shelfCol.size.z) * 0.5f));

        return cornerpoints;
    }

    //Calculates a line between two points and returns the point after a given distance between the two points
    private Vector3 GetPointDistanceFromObject(float distance, Vector3 source, Vector3 target)
    {
        Vector3 directionOfTravel = source - target;
        Vector3 finalDirection = directionOfTravel + directionOfTravel.normalized * distance;
        Vector3 targetPosition = target + finalDirection;

        return targetPosition;
    }

    public GameObject GetGameObjectFromPlaceholder(int row, int column, int listIndex)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;

        //Sucht die liste nach Objekten ab und holt sich den index
        if (pickupHolder.GetComponent<StorageScript>().storageList.Count == 0)
            return null;

        //Debug.Log("FilledBox at " + transform.Find("Lagerplatz " + "R" + bf.row + "C" + bf.column).name);
        return pickupHolder.GetComponent<StorageScript>().GetStorageObjectAtIndex(listIndex);
    }

    public GameObject GetBoxAt(int row, int column)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
        return pickupHolder;
    }

    //returns true if the placeholder contains objects
    public bool GetIsFilledAt(int row, int column)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
        if (pickupHolder.GetComponent<StorageScript>().storageList.Count == 0)
            return false;
        else
            return true;
    }

    public int GetStorageCount()
    {
        return columnCount * rowCount;
    }

    //returns the number of placeholder with objects
    public int GetFilledStorageCount()
    {
        int fillStorageCount = 0;

        for (int i = 1; i <= columnCount; i++)
        {
            for (int j = 1; j <= rowCount; j++)
            {
                if (GetIsFilledAt(j, i))
                    fillStorageCount++;
            }
        }
        return fillStorageCount;
    }

    //delete specific object in the placeholder
    public void DeleteContent(int row, int column, GameObject thisObject)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
        pickupHolder.GetComponent<StorageScript>().DeleteStorageObject(thisObject);
    }

    //TODO: optimize
    //returns the placeholder of the gameobject
    public GameObject GetBoxParent(GameObject gObject)
    {
        GameObject box = null;

        if (gObject == null)
            return null;

        //loop through all placeholder and their children and return the correct parent
        foreach (Transform child in transform)
        {
            if (child.childCount != 0)
            {
                if (child.transform.GetChild(0).gameObject == gObject)
                    box = child.transform.GetChild(0).parent.gameObject;
            }
        }
        return box;
    }

    //returns the row and column from the given object
    public Vector2 GetRowAndColumnFromObject(GameObject gObject)
    {
        Vector2 rowColumn = new Vector2();
        string parentName;

        if (gObject == null)
            return new Vector2(-1, -1);

        //loop through all placeholder and their children and return the correct parent
        foreach (Transform child in transform)
        {
            if (child.transform.GetChild(0).gameObject == gObject)
            {
                parentName = child.transform.GetChild(0).parent.name;
                //get the first character [0] after the Cut [1]
                rowColumn = new Vector2((parentName.Split('R')[1][0] - '0'), (parentName.Split('C')[1][0] - '0')); // - '0' um den korrekten int wert zu erhaltens
            }
        }
        return rowColumn;
    }



    // ---------- Testmethods

    //return how much percent the volume of the placeholder is filled, needs renderer
    public float BoxfilledInPercentage(int row, int column)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
        if (pickupHolder != null)
            return pickupHolder.GetComponent<StorageScript>().BoxfilledInPercentage();
        else
            return -1;
    }

    //find the placeholder and change its size
    public void ChangePlaceHolderSize(int rowChange, int columnChange, Vector3 size)
    {
        //cam.ChangeCameraToNewView()
        if (rowChange < rowCount && columnChange < columnCount)
            transform.Find(("Lagerplatz " + "R" + rowChange + "C" + columnChange)).transform.localScale = size;
        else
            Debug.LogWarning("row & column out of range");

        Debug.Log(("Lagerplatz " + "R" + rowChange + "C" + columnChange) + " is now " + size + " size");
    }
    
    //spawn object at this shelf position
    public void SpawnAtShelfPos(int rowSpawn, int columnSpawn, GameObject objectSpawn, bool spawnSingleObject)
    {
        GameObject pickupHolder = transform.Find(("Lagerplatz " + "R" + rowSpawn + "C" + columnSpawn)).gameObject;

        if (rowSpawn <= rowCount && columnSpawn <= columnCount)
        {
            if (spawnSingleObject)
                pickupHolder.GetComponent<StorageScript>().SpawnSingleAtCenter(objectSpawn);
            else
                pickupHolder.GetComponent<StorageScript>().SpawnOverlapless(objectSpawn);
        }
        else
            Debug.LogWarning("row & column out of range");
    }

    private Vector3 GetObjectAnchor()
    {
        return bottomLeftShelf;
    }

    public GameObject GetEmptyBox(int row, int column)
    {
        GameObject go = transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
        return go;
    }

    public Vector3 GetShelfSize()
    {
        return Vector3.Scale(new Vector3(objectWidth, objectHeight, objectDepth), transform.lossyScale);
    }
}
