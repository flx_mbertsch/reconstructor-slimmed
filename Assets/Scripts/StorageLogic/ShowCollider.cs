﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Put this component on Object to Show the Collider
public class ShowCollider : MonoBehaviour
{
    public bool activateShowCollider = true;
    public Color colliderColor = Color.yellow;

    void OnDrawGizmos()
    {
        if (activateShowCollider)
        {
        Gizmos.color = colliderColor;
        Matrix4x4 cubeTransform = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Matrix4x4 oldGizmosMatrix = Gizmos.matrix;

        Gizmos.matrix *= cubeTransform;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        Gizmos.matrix = oldGizmosMatrix;
        }

        //Gizmos.color = Color.yellow;
        //Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }
}