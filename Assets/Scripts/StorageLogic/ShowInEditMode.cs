﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShowInEditMode : MonoBehaviour
{
    [Header("ObjectSize Size")]
    public Vector3 objectSize;
    public Vector3 placeHolderSize;

    private void Update()
    {
        objectSize = Vector3.Scale(GetComponent<BoxCollider>().size, transform.localScale);
    }
}
