﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
using System.Linq;

//creates procedural mesh and calculates the precise placeholder positions
public class StorageGenerator : MonoBehaviour
{
    [Header("SAP Settings")]
    public string Maske;
    public string PlaceHolderNaming;

    [Header("UserSettings")]
    public float yHeight;
    public int yColumns;
    public int xRows;
    public Vector3 offset;
    public Vector3 placeholderScaling = Vector3.one;
    public bool useMeshAnchors;

    [Header("Appearance Settings")]
    public int betweenBarCount = 1;
    public float highbayPoleThickness;
    public float highbayBarThickness;
    public float highbayBarHeight;
    public Material poleMaterial;
    public Material barMaterial;
    public Material crossMaterial;

    [Header("ShelfMesh Settings")]
    public bool isCreated = false;
    public bool meshMerge = true;
    public LayerMask layerShelf = 9;
    public LayerMask layerPlaceholder = 10;
    public GameObject mainAnchor;
    public GameObject lengthAnchor;
    public GameObject widthAnchor;
    public int zRows;
    public Vector3 mainRotation;
    public float xLength;
    public float zWidth;
    public Vector3 placeholderSize;

    private int namingID;
    private List<Vector3> anchorsPHList = new List<Vector3>();
    private GameObject createdShelf;

    Vector3 gizmoPoint;
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.TransformPoint(placeholderSize), 0.5f);
    }

    private void Awake()
    {
        layerShelf = LayerMask.GetMask("Shelf");
        layerPlaceholder = LayerMask.GetMask("Placeholder");
    }

    //TODO: struct/interfaces for setter methods
    public void StorageGeneratorSetterGUI(float yHeight, int yColumns, int xRows, Vector3 placeholderScaling,
        int betweenBarCount, float highbayPoleThickness, float highbayBarThickness, float highbayBarHeight,
        Material poleMaterial, Material barMaterial, Material crossMaterial)
    {
        this.yHeight = yHeight;
        this.yColumns = yColumns;
        this.xRows = xRows;
        this.placeholderScaling = placeholderScaling;
        this.betweenBarCount = betweenBarCount;
        this.highbayPoleThickness = highbayPoleThickness;
        this.highbayBarThickness = highbayBarThickness;
        this.highbayBarHeight = highbayBarHeight;
        //TODO: verbuggt
        //this.poleMaterial = poleMaterial;
        //this.barMaterial = barMaterial;
        //this.crossMaterial = crossMaterial;
    }

    //SingleMesh shelf: creates shelf with mesh and placeholder
    public void SpawnNewHighbay(Vector3 position, Vector3 rotation, int rows, int columns, int depth, float length, float width, float height)
    {
        createdShelf = new GameObject("HighbayStorage " + namingID);
        namingID++;
        createdShelf.transform.position = position;
        createdShelf.transform.eulerAngles = rotation;

        //creates the shelf mesh
        MeshFilter mf = createdShelf.AddComponent<MeshFilter>();
        MeshRenderer mr = createdShelf.AddComponent<MeshRenderer>();
        mf.mesh = CreateHighbayMesh(rows, columns, depth, length, width, height);
        createdShelf.GetComponent<MeshRenderer>().material = poleMaterial;

        isCreated = true;
        //Set all new shelfs as child of this parent
        GameObject parent = GameObject.Find("Lagerobjekte") == null ? new GameObject("Lagerobjekte") : GameObject.Find("Lagerobjekte");
        createdShelf.transform.SetParent(parent.transform);

        AddPlaceholder(createdShelf, rows, columns);

        // Allow the use of Undo (Ctrl+Z, Ctrl+Y).
        Undo.RegisterCreatedObjectUndo(createdShelf, "");
    }

    //SingleMesh
    private Mesh CreateHighbayMesh(int rows, int columns, int depth, float length, float width, float height)
    {
        Mesh outputMesh = new Mesh();

        List<Vector3> combinedVertices = new List<Vector3>();
        combinedVertices.AddRange(CreatePoleVertices(columns, depth, length, width, height));
        combinedVertices.AddRange(CreateBarVertices(rows, depth, length, width, height));
        combinedVertices.AddRange(CreateCrossVertices(rows, columns, length, width, height));

        List<int> combinedTriangles = CreateCubeTrianglesForVertexList(combinedVertices);

        outputMesh.vertices = combinedVertices.ToArray();
        outputMesh.triangles = combinedTriangles.ToArray();
        outputMesh.RecalculateNormals();
        outputMesh.RecalculateTangents();

        return outputMesh;
    }

    //Multiple Materials
    public void SpawnMultiColorHighbay(Vector3 position, Vector3 rotation, int rows, int columns, int depth, float length, float width, float height)
    {
        createdShelf = new GameObject("Highbay " + namingID);
        namingID++;
        createdShelf.transform.position = position;
        createdShelf.transform.eulerAngles = rotation;
        //Set all new storages as child of a parent
        GameObject parent = GameObject.Find("Lagerobjekte") ?? new GameObject("Lagerobjekte");
        createdShelf.transform.SetParent(parent.transform);
        MeshFilter mfHighbay = createdShelf.AddComponent<MeshFilter>();
        MeshRenderer mrHighBay = createdShelf.AddComponent<MeshRenderer>();
        List<Mesh> submeshes = CreateHighbaySubMeshes(rows, columns, depth, length, width, height);

        GameObject poleSpawn = new GameObject("Poles");
        poleSpawn.AddComponent<MeshFilter>().mesh = submeshes[0];
        poleSpawn.AddComponent<MeshRenderer>().material = poleMaterial;
        poleSpawn.transform.SetParent(createdShelf.transform);

        GameObject barSpawn = new GameObject("Bars");
        barSpawn.AddComponent<MeshFilter>().mesh = submeshes[1];
        barSpawn.AddComponent<MeshRenderer>().material = barMaterial;
        barSpawn.transform.SetParent(createdShelf.transform);

        GameObject crossSpawn = new GameObject("Crosses");
        crossSpawn.AddComponent<MeshFilter>().mesh = submeshes[2];
        crossSpawn.AddComponent<MeshRenderer>().material = crossMaterial;
        crossSpawn.transform.SetParent(createdShelf.transform);

        //TODO: auch ohne MeshMerge placeholder setzen
        //Combine Meshes and Materials from all childs
        if (meshMerge)
        {
            MeshMergeMaterials(mfHighbay, mrHighBay);
            AddPlaceholder(createdShelf, rows, columns);
        }
    }

    //Combines all child meshes and materials to one mesh with submeshes and materials
    private void MeshMergeMaterials(MeshFilter mfHighbay, MeshRenderer mrHighBay)
    {
        Mesh finalMesh = new Mesh();
        MeshFilter[] mFilters = mfHighbay.GetComponentsInChildren<MeshFilter>();
        //For each submesh one material
        List<Material> mList = new List<Material>(mFilters.Length);
        //creates a list of materials and pass it to the new mesh
        CombineInstance[] combine = new CombineInstance[mFilters.Length];

        //iterate through all MeshFilter and comines their meshes
        //start at 1 to skip parent
        for (int a = 1; a < mFilters.Length; a++)
        {
            combine[a].subMeshIndex = 0;
            combine[a].mesh = mFilters[a].sharedMesh;
            combine[a].transform = mFilters[a].transform.localToWorldMatrix;
            //Colors are added in the order the submesh got created
            mList.Add(mFilters[a].gameObject.GetComponent<MeshRenderer>().sharedMaterial);
            //Destroy the child object after its merge
            DestroyImmediate(mFilters[a].gameObject);
        }

        //Skips first array entry which became null after its parent destruction
        combine = combine.Skip(1).ToArray();
        //Dont merge material list from the submeshes and create an own
        finalMesh.CombineMeshes(combine, false);
        mfHighbay.sharedMesh = finalMesh;
        mrHighBay.materials = mList.ToArray();

        isCreated = true;
        // Allow the use of Undo (Ctrl+Z, Ctrl+Y).
        Undo.RegisterCreatedObjectUndo(finalMesh, "");
    }

    //Multiple Materials SubMeshes
    private List<Mesh> CreateHighbaySubMeshes(int rows, int columns, int depth, float length, float width, float height)
    {
        List<Mesh> outputMeshes = new List<Mesh>();

        Mesh poleMesh = new Mesh();
        List<Vector3> poleVertices = CreatePoleVertices(columns, depth, length, width, height);
        poleMesh.SetVertices(poleVertices);
        poleMesh.SetTriangles(CreateCubeTrianglesForVertexList(poleVertices), 0);
        poleMesh.RecalculateNormals();
        //poleMesh.triangles = poleMesh.triangles.Reverse().ToArray();
        outputMeshes.Add(poleMesh);

        Mesh barMesh = new Mesh();
        List<Vector3> barVertices = CreateBarVertices(rows, depth, length, width, height);
        barMesh.SetVertices(barVertices);
        barMesh.SetTriangles(CreateCubeTrianglesForVertexList(barVertices), 0);
        barMesh.triangles = barMesh.triangles.Reverse().ToArray();
        barMesh.RecalculateNormals();
        outputMeshes.Add(barMesh);

        Mesh crossMesh = new Mesh();
        List<Vector3> crossVertices = CreateCrossVertices(rows, columns, length, width, height);
        crossMesh.SetVertices(crossVertices);
        crossMesh.SetTriangles(CreateCubeTrianglesForVertexList(crossVertices), 0);
        //Flip all normals
        crossMesh.RecalculateNormals();
        //crossMesh.triangles = crossMesh.triangles.Reverse().ToArray();
        outputMeshes.Add(crossMesh);

        return outputMeshes;
    }

    //Basic Utilities
    private List<Vector3> CreatePoleVertices(int columns, int depth, float length, float width, float height)
    {
        length -= highbayPoleThickness;
        width -= highbayPoleThickness;

        List<Vector3> output = new List<Vector3>();
        Vector3 offset = Vector3.zero;

        float xIntervall = length / columns;
        float zIntervall = width / depth;

        for (int i = 0; i <= columns; i++)
        {
            offset.x = xIntervall * i;
            for (int a = 0; a <= depth; a++)
            {
                offset.z = zIntervall * a;
                output.AddRange(CreateCubeVertices(offset, height, highbayPoleThickness, highbayPoleThickness));
            }
        }
        return output;
    }

    private List<Vector3> CreateBarVertices(int rows, int depth, float length, float width, float height)
    {
        length -= (highbayPoleThickness + highbayBarThickness);
        width -= highbayPoleThickness;
        height -= highbayBarThickness;

        List<Vector3> output = new List<Vector3>();
        Vector3 offset = new Vector3(highbayPoleThickness / 2, 0, 0);

        float yIntervall = height / rows;
        float zIntervall = width / depth;
        float zOffset = highbayPoleThickness / 2 - highbayBarThickness / 2;

        for (int i = 1; i <= rows; i++)
        {
            offset.y = yIntervall * i - highbayBarHeight / 2;
            for (int a = 0; a <= depth; a++)
            {
                offset.z = zIntervall * a + zOffset;
                output.AddRange(CreateCubeVertices(offset, highbayBarHeight, length, highbayBarThickness));
            }
        }

        return output;
    }

    private List<Vector3> CreateCrossVertices(int rows, int columns, float length, float width, float height)
    {
        length -= (highbayPoleThickness + highbayBarThickness);
        width -= (highbayPoleThickness + highbayBarThickness);
        height -= highbayBarThickness;

        //Set the Size of a placeholder from the shelfMesh minus the Pole-/bar-/cross-thickness
        placeholderSize = new Vector3(((-length - highbayBarThickness) / columns) - highbayPoleThickness, (height / rows) - highbayBarThickness, width + highbayBarThickness * 2);

        List<Vector3> output = new List<Vector3>();
        Vector3 offset = new Vector3(0, 0, highbayPoleThickness / 2 + highbayBarThickness / 2);

        float yIntervall = height / rows;
        float xIntervall = length / columns / betweenBarCount;

        for (int i = 1; i <= rows; i++)
        {
            offset.y = yIntervall * i - highbayBarHeight / 2;
            for (float a = 0f; a <= columns * betweenBarCount; a += 0.5f)
            {
                offset.x = xIntervall * a + (highbayBarThickness / ((columns * betweenBarCount) / a)) + highbayBarThickness / 2;
                output.AddRange(CreateCubeVertices(offset, highbayBarHeight, highbayBarThickness, width));
                //set the anchorpooint from each placeholder at the bottom left vertice, except from the last vertice
                if (a % betweenBarCount == 0 && a < columns * betweenBarCount)
                {
                    //xShift: half the bar size to the left and additional half the pole size because of the smaller proportions of the placeholder
                    //yShift: To start at the ground subtract yIntervall
                    anchorsPHList.Add(new Vector3(offset.x + highbayBarThickness / 2 - highbayPoleThickness / 2, offset.y - yIntervall + highbayBarThickness, highbayPoleThickness / 4));
                }
            }
        }
        return output;
    }

    private List<Vector3> CreateCubeVertices(Vector3 offset, float height, float length, float thickness)
    {
        return new List<Vector3> {
            offset + new Vector3(0, 0, 0),
            offset + new Vector3(0, 0, 0),
            offset + new Vector3(0, 0, 0),
            offset + new Vector3(length, 0, 0),
            offset + new Vector3(length, 0, 0),
            offset + new Vector3(length, 0, 0),
            offset + new Vector3(0, 0, thickness),
            offset + new Vector3(0, 0, thickness),
            offset + new Vector3(0, 0, thickness),
            offset + new Vector3(length, 0, thickness),
            offset + new Vector3(length, 0, thickness),
            offset + new Vector3(length, 0, thickness),
            offset + new Vector3(0, height, 0),
            offset + new Vector3(0, height, 0),
            offset + new Vector3(0, height, 0),
            offset + new Vector3(length, height, 0),
            offset + new Vector3(length, height, 0),
            offset + new Vector3(length, height, 0),
            offset + new Vector3(0, height, thickness),
            offset + new Vector3(0, height, thickness),
            offset + new Vector3(0, height, thickness),
            offset + new Vector3(length, height, thickness),
            offset + new Vector3(length, height, thickness),
            offset + new Vector3(length, height, thickness),
        };
    }

    private List<int> CreateCubeTriangles(int offset)
    {
        return new List<int>()
        {
            0 + offset, 3 + offset, 6 + offset, 6 + offset, 3 + offset, 9 + offset,   //bottom
            1 + offset, 12 + offset, 4 + offset, 12 + offset, 15 + offset, 4 + offset,   //front
            7 + offset, 18 + offset, 2 + offset, 18 + offset, 13 + offset, 2 + offset,   //left
            10 + offset, 21 + offset, 8 + offset, 21 + offset, 19 + offset, 8 + offset,   //back
            5 + offset, 16 + offset, 11 + offset, 16 + offset, 22 + offset, 11 + offset,   //right
            14 + offset, 20 + offset, 17 + offset, 20 + offset, 23 + offset, 17 + offset    //top
        };
    }

    private List<int> CreateCubeTrianglesForVertexList(List<Vector3> vertices)
    {
        List<int> output = new List<int>();

        for (int i = 0; i < vertices.Count; i += 24)
        {
            output.AddRange(CreateCubeTriangles(i));
        }

        return output;
    }

    //Add placeholder at the anchor points
    public void AddPlaceholder(GameObject shelfObject, int rows, int columns)
    {
        BoxCollider shelfCol = shelfObject.AddComponent<BoxCollider>();
        shelfCol.isTrigger = true;
        PlaceholderGenerator cPlaceHolder = shelfObject.AddComponent<PlaceholderGenerator>();
        shelfObject.layer = layerShelf;

        CalculatePlaceHolder(cPlaceHolder, rows, columns, shelfCol);
    }

    //Calculates with the given Data the positions and size of the Placeholder
    public void CalculatePlaceHolder(PlaceholderGenerator cPlaceHolder, int rows, int columns, BoxCollider shelfCol)
    {
        //set the placeholder at the anchor point list
        if (useMeshAnchors)
        {
            cPlaceHolder.useMeshAnchors = useMeshAnchors;
            cPlaceHolder.anchorsPHList = anchorsPHList;
            cPlaceHolder.columnCount = rows;
            cPlaceHolder.rowCount = columns;
            cPlaceHolder.placeholderSize = placeholderSize;
            cPlaceHolder.CreateStorages();
        }
        //calculate the placeholder with the shelf size
        else
        {
            cPlaceHolder.PlaceHolderParameterSetter(rows, columns, placeholderScaling, offset, placeholderSize, layerPlaceholder);
            cPlaceHolder.CreateStorages();
        }
    }

    public void CreateShelf()
    {
        CreateShelfInternal();
        //Destroy parent anchors
        DestroyImmediate(transform.gameObject);
    }

    //TODO: Bessere Möglichkeit für Überladen finden
    public void CreateShelf(out GameObject createdObject)
    {
        CreateShelfInternal();
        createdObject = createdShelf;
        //Destroy parent anchors
        DestroyImmediate(transform.gameObject);
    }

    //To hand over anchor parameter
    public void CreateShelf(out GameObject createdObject, Vector3 mainAnchorPos, Vector3 lengthAnchorPos, Vector3 widthAnchorPos, Vector3 rotation, int hbID)
    {
        namingID = hbID;
        CreateShelfInternal(mainAnchorPos, lengthAnchorPos, widthAnchorPos);
        createdShelf.transform.rotation = Quaternion.Euler(rotation);
        createdObject = createdShelf;
        //Destroy parent anchors
        DestroyImmediate(transform.gameObject);
    }

    private void CreateShelfInternal()
    {
        //Update values for anchorsPos
        if (mainAnchor != null)
        {
            mainRotation = mainAnchor.transform.eulerAngles;
        }
        if (lengthAnchor != null)
        {
            xLength = lengthAnchor.transform.localPosition.x;
        }
        if (widthAnchor != null)
        {
            zWidth = widthAnchor.transform.localPosition.z;
        }

        SpawnMultiColorHighbay(mainAnchor.transform.position, mainRotation, yColumns, xRows, zRows, xLength, zWidth, yHeight);

        //Copy component to the new created shelf with its values
        //using the UnityEditor functions
        UnityEditorInternal.ComponentUtility.CopyComponent(GetComponent<StorageGenerator>());
        UnityEditorInternal.ComponentUtility.PasteComponentAsNew(createdShelf);
    }

    private void CreateShelfInternal(Vector3 mainAnchorPos, Vector3 lengthAnchorPos, Vector3 widthAnchorPos)
    {
        xLength = lengthAnchorPos.x;
        zWidth = widthAnchorPos.z;

        SpawnMultiColorHighbay(mainAnchorPos, Vector3.zero, yColumns, xRows, zRows, xLength, zWidth, yHeight);

        //Copy component to the new created shelf with its values
        //using the UnityEditor functions
        UnityEditorInternal.ComponentUtility.CopyComponent(GetComponent<StorageGenerator>());
        UnityEditorInternal.ComponentUtility.PasteComponentAsNew(createdShelf);
    }

    private void TestCreation()
    {
        CreateShelf();
    }
}

