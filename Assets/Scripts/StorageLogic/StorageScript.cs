﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//TODO: erben von StorageGenerator und PlaceholderGenerator

//Script to handle the objects inside a placeholder
public class StorageScript : MonoBehaviour
{
    Vector3 objectSize;
    //track the size and anchorpoints of stored objects
    public Vector3 maxAnchorGuide;
    public Vector3 lastObjAnchor;

    public List<GameObject> storageList = new List<GameObject>();
    public static BoxCollider thisPHCol = null;

    private static Vector3 behindLeftPlaceHolder;
    private int testID = 0;
    private Vector3 topRightObjTest;
    private Vector3 bottomLeft;

    private void OnEnable()
    {
        objectSize = Vector3.Scale(GetComponent<BoxCollider>().size, transform.localScale);
    }

    //Tests: Alle anchors vom Nullpunkt zum Objekt vershiftet mit TransformPoint
    //Rot = maxAnchorVolumen
    //Grün = untere linke Ecke Platzhalter
    //Weiss = letzter anchor point
    private void OnDrawGizmos()
    {
        ////shows volume of filledVolume
        ////shifts gizmo to corner overlap with the PH
        ////Berechnet maxAnchor in Worldrelation am unterne Linken Rand der Box und shiftet es um ein halbes Objekt in die Mitte.
        //Gizmos.color = Color.red;
        //Vector3 rightShift = new Vector3(-maxAnchorGuide.x / 2, maxAnchorGuide.y / 2, maxAnchorGuide.z / 2);
        //Vector3 worldScaling = Vector3.Scale(maxAnchorGuide, transform.localScale);
        //Matrix4x4 cubeTransform = Matrix4x4.TRS(transform.TransformPoint(behindLeftPlaceHolder + rightShift), transform.rotation, worldScaling);
        //Matrix4x4 oldGizmosMatrix = Gizmos.matrix;

        //Gizmos.matrix *= cubeTransform;
        //Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        //Gizmos.matrix = oldGizmosMatrix;

        ////shows anchorpoint from the next spawning object
        //Gizmos.color = Color.white;
        //Gizmos.DrawSphere(transform.TransformPoint(lastObjAnchor), 0.2f);
        ////Gizmos.color = Color.cyan;
        ////Gizmos.DrawSphere(transform.TransformPoint(topRightObjTest), 0.2f);

        ////Draw obj collider
        //Gizmos.color = Color.green;
        //Gizmos.DrawSphere(transform.TransformPoint(behindLeftPlaceHolder), 0.2f);
        ////Gizmos.DrawSphere(testObj.transform.TransformPoint( GetBehindLeftCornerPosition(testObj)), 0.2f);

        //Gizmos.DrawLine(transform.TransformPoint(behindLeftPlaceHolder), transform.TransformPoint(lastObjAnchor));
    }

    //TODO: Diese Methode nur laufen lassen, wenn das Objekt erstellt wurde
    public void CalculateCorner()
    {
        thisPHCol = GetComponent<BoxCollider>();
        //behind bottom left placeHolder, Anchorpoint
        behindLeftPlaceHolder = GetBehindLeftCornerPosition(this.gameObject);
        lastObjAnchor = behindLeftPlaceHolder;
    }

    //set the object at the correct position as top child
    public void SpawnOverlapless(GameObject objectSpawn)
    {
        //TODO: Optimize so the next two lines arent needed anymore
        if (thisPHCol == null)
        {
            thisPHCol = GetComponent<BoxCollider>();
            behindLeftPlaceHolder = GetBehindLeftCornerPosition(this.gameObject);
        }

        GameObject tempObject = Instantiate(objectSpawn, Vector3.zero, transform.rotation, transform) as GameObject;
        //TODO: Optimize 
        tempObject.transform.localScale = new Vector3(tempObject.transform.localScale.x / transform.lossyScale.x, tempObject.transform.localScale.y / transform.lossyScale.y, tempObject.transform.localScale.z / transform.lossyScale.z);

        tempObject.name = objectSpawn.name + " " + testID.ToString();
        tempObject.transform.SetAsFirstSibling(); //set it first childobject
        storageList.Add(tempObject);

        bottomLeft = GetBehindLeftCornerPosition(tempObject);
        CalculateAnchors(tempObject);
        testID++;

        // Allow the use of Undo (Ctrl+Z, Ctrl+Y)
        if (tempObject != null)
            Undo.RegisterCreatedObjectUndo(tempObject, "");
    }

    //Spawn object always at the centre
    public void SpawnSingleAtCenter(GameObject objectSpawn)
    {
        //Pivot ist unten mittig
        Vector3 shiftToMiddle = transform.TransformPoint(behindLeftPlaceHolder) + Vector3.Scale(new Vector3(-thisPHCol.size.x / 2, 0, thisPHCol.size.z / 2), transform.lossyScale);

        //Spawns object in Center of Box
        GameObject tempObject = Instantiate(objectSpawn, shiftToMiddle, transform.rotation, transform) as GameObject;
        //tempObject.name = ("HU Nummer" + transform.name);
        //create random number to look like HU nummer
        int rndNumber = Random.Range(100, 999);
        tempObject.name = ("Karton HU " + rndNumber);
        tempObject.transform.SetAsFirstSibling(); //setzt es als erstes child object
        storageList.Add(tempObject);

        lastObjAnchor = transform.InverseTransformPoint(transform.position);

        // Allow the use of Undo (Ctrl+Z, Ctrl+Y).
        Undo.RegisterCreatedObjectUndo(tempObject, "");
    }

    //TODO: Test size and rotation for different objects
    //TODO: Performance: Wenn offensichtlich ist, dass das neue Objekt nicht reinpasst. Erst gar nicht spawnen und den Platztest durchführen (Volumenvergleich vorher)
    //add new Volume to the right position, and set after which object the next one should be added 
    //check if object fits on the right
    //then check if it fits on top left
    //then check if it fits on front of the last row left
    //place object if possible
    private void CalculateAnchors(GameObject addObj)
    {
        int caseSwitch = -1;

        Vector3 objWorldSize = addObj.GetComponent<BoxCollider>().size;
        Vector3 objLocalSize = Vector3.Scale(objWorldSize, addObj.transform.localScale);

        Vector3 topRightObj = lastObjAnchor + new Vector3(-objLocalSize.x, objLocalSize.y, objLocalSize.z);
        topRightObjTest = topRightObj;

        //first of all check if the object fit in the placeholder
        if (!ColliderContainsPoint(transform, transform.TransformPoint(behindLeftPlaceHolder + maxAnchorGuide + topRightObj)))
            caseSwitch = 4;
        //if there is no existing volume 
        else if (maxAnchorGuide == Vector3.zero)
            caseSwitch = 0;
        //as long it fits inside the placeholder shift to the right (-x)
        else if (maxAnchorGuide.x + objLocalSize.x <= thisPHCol.size.x)
            caseSwitch = 1;
        //if the object fits at the top of the last row
        else if (maxAnchorGuide.y + objLocalSize.y <= thisPHCol.size.y)
            caseSwitch = 2;
        //if the object fits in front of the last row
        else if (maxAnchorGuide.z + objLocalSize.z <= thisPHCol.size.z)
            caseSwitch = 3;
        //if there is no place in the placeholder for the object
        else
            caseSwitch = 4;

        switch (caseSwitch)
        {
            case 0:
                //anchorgoide is always the volume of the biggest object in the row and set the anchorpoint right besides it
                SetAtAnchorPos(addObj, lastObjAnchor);
                maxAnchorGuide = objLocalSize;
                //start at the bottom left of the placeholder
                lastObjAnchor = behindLeftPlaceHolder + new Vector3(-objLocalSize.x, 0, 0);
                break;
            case 1:
                //no matter what the lengthiest object ist, always set anchor to the right of the last object
                SetAtAnchorPos(addObj, lastObjAnchor);
                //if the anchor of the added object would be outside of the placeholder, reset the anchorguide and expand it with xObjectsize
                maxAnchorGuide = new Vector3(maxAnchorGuide.x + objLocalSize.x, maxAnchorGuide.y, maxAnchorGuide.z);

                //check if y or z is bigger than the previous biggest object in the row, if so set the values to the new object
                if (maxAnchorGuide.y < objLocalSize.y)
                    maxAnchorGuide.y = objLocalSize.y;

                if (maxAnchorGuide.z < objLocalSize.z)
                    maxAnchorGuide.z = objLocalSize.z;

                //shift to the right with the x size of the Object
                lastObjAnchor = lastObjAnchor + new Vector3(-objLocalSize.x, 0, 0);
                break;
            case 2:
                //shift the object to top left from maxAnchorguide (highest object of the previous row)
                //recalculate world maxAnchorGuide to localSize and subtract the current size to put the box on top of  the same row at the previous row max height
                lastObjAnchor = behindLeftPlaceHolder + new Vector3(0, maxAnchorGuide.y, maxAnchorGuide.z - objLocalSize.z);
                SetAtAnchorPos(addObj, lastObjAnchor);

                maxAnchorGuide = new Vector3(objLocalSize.x, maxAnchorGuide.y + objLocalSize.y, maxAnchorGuide.z);
                //set the new anchorpoint besides the current added object
                lastObjAnchor = lastObjAnchor + new Vector3(-objLocalSize.x, 0, 0);
                break;
            case 3:
                //reset anchorguide to the bottom of the placeholder and add the rowWidth with the objectWidth
                //shift to the bottom left of the last placeholder 
                lastObjAnchor = behindLeftPlaceHolder + new Vector3(0, 0, maxAnchorGuide.z);
                SetAtAnchorPos(addObj, lastObjAnchor);
                maxAnchorGuide = new Vector3(objLocalSize.x, 0, maxAnchorGuide.z + objLocalSize.z);
                //set the new anchorpoint besides the added object
                lastObjAnchor = lastObjAnchor + new Vector3(-objLocalSize.x, 0, 0);
                break;
            case 4:
                //Debug.Log("Kein Platz mehr in diesem Platzhalter: " + transform.name);
                Debug.Log("Kein Platz mehr in diesem Platzhalter: ");
                //object has to be first created to calculate its size and if it fits in the placeholder
                //therefore it can only destroyed after the checks above
                DeleteStorageObject(addObj);
                break;
            default:
                Debug.Log("Probleme bei Switch -ADDfilledVolume-");
                break;
        }
        //Debug.Log("case " + caseSwitch);
    }

    //Calculate WITHOUT axis aligned bounding box (AABB) the point inside of the box -> rotation stable
    private bool ColliderContainsPoint(Transform ColliderTransform, Vector3 Point)
    {
        Vector3 localPos = ColliderTransform.InverseTransformPoint(Point);
        if (Mathf.Abs(Point.x) <= 1f && Mathf.Abs(Point.y) <= 1f && Mathf.Abs(Point.z) <= 1f)
            return true;
        else
            return true;
    }

    //Localposition
    private Vector3 GetBehindLeftCornerPosition(GameObject go)
    {
        BoxCollider goCollider = go.GetComponent<BoxCollider>();
        Vector3 behindLeftPos = goCollider.center + new Vector3(goCollider.size.x, -goCollider.size.y, -goCollider.size.z) * 0.5f;
        return behindLeftPos;
    }

    //set the object at the right position bottom left
    //calculates the bottom left position of the BoxCollider of the object and spawn the new object aligned to the last BoxCollider
    //shift the object half a size to the middle so it fits with the anchorpoints
    public void SetAtAnchorPos(GameObject addObj, Vector3 anchorPos)
    {
        BoxCollider addObjColl = addObj.GetComponent<BoxCollider>();
        Vector3 objSize = Vector3.Scale(addObjColl.size, addObj.transform.localScale);

        //shift the object to the middle of the objectcollider
        Vector3 objRightShift = anchorPos + new Vector3(-objSize.x / 2, objSize.y / 2, objSize.z / 2);
        addObj.transform.localPosition = objRightShift;
    }

    //TODO: Problem: Wenn das gelöschte Objekt das höchste bzw längste war, bleibt beim Löschen der maxAnchor auf diese Größe
    //Delete the object and set its anchor to bottom right point of the next object
    public void DeleteLIFO()
    {
        GameObject lastObj = GetLatestStoredObject();
        //if the placeholder is empty, abort the method
        if (lastObj == null)
        {
            Debug.Log("Platzhalter ist leer");
            return;
        }

        Vector3 objWorldSize = lastObj.GetComponent<BoxCollider>().size;
        Vector3 objLocalSize = Vector3.Scale(objWorldSize, lastObj.transform.localScale);

        //set the anchor to the bottom left of the object in the relation to the parentScale
        lastObjAnchor = lastObj.transform.localPosition;
        lastObjAnchor += Vector3.Scale(GetBehindLeftCornerPosition(lastObj), lastObj.transform.localScale);

        //Calculate the volume of the anchor point and add the size to the last object 
        maxAnchorGuide = lastObjAnchor - behindLeftPlaceHolder;
        //shift tho the right (-X) of the maxAnchor
        maxAnchorGuide = new Vector3(-maxAnchorGuide.x, maxAnchorGuide.y, maxAnchorGuide.z);
        maxAnchorGuide += objLocalSize - new Vector3(objLocalSize.x, 0, 0);

        //Debug.Log("Deleted Object " + lastObj.name);
        DeleteStorageObject(lastObj);
    }

    public void SetStorageObject(GameObject otherGameObject)
    {
        storageList.Add(otherGameObject);
        //Debug.Log("STORAGE OBJECT SET !!" + "\n" + "called by Storage Name: " + transform.name + "\n" + "contains Elements: " + storageList.Count);
    }

    //Destroy the object and delete it from the storagelist
    public void DeleteStorageObject(GameObject otherGameObject)
    {
        //Debug.Log("Try to delete " + otherGameObject.name +"!!" + "\n" + "called by Storage Name: " + transform.name + "\n" + "contains Elements: " + storageList.Count);
        if (storageList.Contains(otherGameObject))
        {
            storageList.Remove(otherGameObject);
            DestroyImmediate(otherGameObject);
            //Debug.Log("OBJECT DELETED : " + otherGameObject.name + "!!" + "\n" + "Storage Name: " + transform.name + "\n" + "contains Elements: " + storageList.Count);
        }
        else
            Debug.Log("Box not containing this object");
    }

    //the last inserted object in the objectlist
    public GameObject GetLatestStoredObject()
    {
        //Debug.Log("GET LATEST OBJECT !!" + "\n" + "called by Storage Name: " + transform.name + "\n" + "contains Elements: " + storageList.Count);
        if (storageList.Count == 0)
            return null;
        else
            return storageList[storageList.Count - 1];
    }

    public bool BoxContainsObj(GameObject obj)
    {
        return storageList.Contains(obj);
    }

    public GameObject GetStorageObjectAtIndex(int index)
    {
        return storageList[index];
    }

    public bool CheckColliderPlaceHolderIntersect(GameObject goIntersect)
    {
        if (transform.GetComponent<BoxCollider>().bounds.Intersects(goIntersect.GetComponent<BoxCollider>().bounds))
            return true;
        else
            return false;
    }

    //returns if this placeholder is filled
    public bool GetIsFilled()
    {
        if (storageList.Count == 0)
            return false;
        else
            return true;
    }

    //returns how much percentage the volume of the placeholder is filled with objects, needs renderer
    //calculates all boxcollider volumes together and divide them through the placeholder volume
    public float BoxfilledInPercentage()
    {
        Vector3 gObject;
        Vector3 placeHolder;
        float endVolume = 0;
        float volumePh;
        float volumeObjects;

        if (storageList.Count == 0)
            return 0;
        else
        {
            foreach (GameObject o in storageList)
            {
                gObject = o.GetComponent<Collider>().bounds.size;

                placeHolder = transform.GetComponent<Collider>().bounds.size;
                //Calculates the distance and translate it to percentage
                volumePh = placeHolder.x * placeHolder.y * placeHolder.z;
                volumeObjects = gObject.x * gObject.y * gObject.z;
                endVolume += 100 / (volumePh / volumeObjects);
            }
            return endVolume;
        }
    }

    public void DeleteAllPlaceholder()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
    }
}
