﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//UI class for overlay informations and buttons
public class ObjectInfo : MonoBehaviour
{
    public GameObject textObject;
    public GameObject testBoxes;
    public GameObject shelfButton;
    public GameObject placeholderButton;
    public GameObject volumeButton;

    private TextMeshProUGUI textUI;
    private IgnoreCast ignoreRaycastRegal = new IgnoreCast(null, 0);
    private IgnoreCast ignoreRaycastPlatzhalter = new IgnoreCast(null, 0);
    private GameObject ignoreRaycastMaterial;

    private void Start()
    {
        textUI = textObject.GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //check if mouse is over a UI
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    Debug.Log("hit object " + hit.transform.gameObject.name);
                    GameObject hitObject = hit.transform.gameObject;
                    ChangeText(hitObject);
                }
            }
        }

        //Deselecte the object but stay in the placeholder
        if (Input.GetMouseButtonUp(2))
        {
            Debug.Log("clicked");
            ResetLayers(10);
        }
    }

    string topObject = "";
    string platzhalterObject = "";
    string materialObject = "";
    //layer 9 = Regale, layer 10 = Platzhalter
    private void ChangeText(GameObject go)
    {
        //Falls es kein Logistikobjekt ist setzt den Namen zurück aber behält das letzte tempIgnoreRaycast
        //if the selected object is not a logisticsobject reset the layers
        if (go.layer < 9)
        {
            Debug.Log("Switch 1");
            ResetLayers(9);
            topObject = go.name;
        }
        //is it a regal layer? is it already ignored?
        else if (go.layer == 9 && go != ignoreRaycastRegal.go)
        {
            Debug.Log("Switch 2");
            ResetLayers(9);
            ignoreRaycastRegal = new IgnoreCast(go, go.layer);
            go.layer = 2;
            topObject = go.name;
        }
        //is it a platzhalter layer? is it already ignored?
        else if (go.layer == 10 && go != ignoreRaycastPlatzhalter.go)
        {
            Debug.Log("Switch 3");
            ResetLayers(10);
            ignoreRaycastPlatzhalter = new IgnoreCast(go, go.layer);
            go.layer = 2;
            platzhalterObject = "<color=#32CD32>" + go.name + "</color>\n";
        }
        //if the object is already selected, ignore it
        else if (go != ignoreRaycastMaterial)
        {
            Debug.Log("Switch 4");
            ResetLayers(11);
            ignoreRaycastMaterial = go;
            materialObject = "<color=#FF0000>" + go.name + "</color>\n";
        }
        textUI.text = topObject + "\n" + platzhalterObject + "\n" + materialObject;

        if (ignoreRaycastRegal.go == null)
            volumeButton.GetComponent<Button>().interactable = false;
        else
            volumeButton.GetComponent<Button>().interactable = true;
    }

    //Dont forget to add "content size fitter" and "vertical layout group" Component to content
    //reset the object to their layer
    public void ResetLayers(int layer)
    {
        switch (layer)
        {
            case 9:
                if (ignoreRaycastRegal.go != null)
                    ignoreRaycastRegal.go.layer = ignoreRaycastRegal.layerSave;
                if (ignoreRaycastPlatzhalter.go != null)
                    ignoreRaycastPlatzhalter.go.layer = ignoreRaycastPlatzhalter.layerSave;

                ignoreRaycastRegal.go = null;
                topObject = "";
                platzhalterObject = "";
                materialObject = "";
                break;
            case 10:
                if (ignoreRaycastPlatzhalter.go != null)
                    ignoreRaycastPlatzhalter.go.layer = ignoreRaycastPlatzhalter.layerSave;

                platzhalterObject = "";
                materialObject = "";
                break;
        }
    }

    public void AddBoxButton()
    {
        if (ignoreRaycastPlatzhalter.go != null)
            ignoreRaycastPlatzhalter.go.GetComponent<StorageScript>().SpawnOverlapless(testBoxes);
        else
            Debug.Log("Kein Platzhalter ausgewählt");
    }

    public void DeleteButton()
    {
        if (ignoreRaycastPlatzhalter.go != null)
            ignoreRaycastPlatzhalter.go.GetComponent<StorageScript>().DeleteLIFO();
        else
            Debug.Log("Kein Platzhalter ausgewählt");
    }

    //creates a scrollview list grid with buttons and shelf place structure
    public void FillScrollView()
    {
        //reset shelfinfos
        GameObject placeHolder;
        GameObject shelfObject;
        PlaceholderGenerator shelfPH;

        //Only if a shelf is selected
        if (ignoreRaycastRegal.go != null)
        {
            shelfObject = ignoreRaycastRegal.go;
            shelfPH = shelfObject.GetComponent<PlaceholderGenerator>();
            shelfButton.GetComponentInChildren<Text>().text = shelfObject.name;

            //loopt through all placeholder of the shelf and creates corresponding buttons
            for (int column = 1; column <= shelfPH.columnCount; column++)
            {
                for (int row = 1; row <= shelfPH.rowCount; row++)
                {
                    //set the existing button as the first placeholder
                    if (row == 1 && column == 1)
                    {
                        placeholderButton.SetActive(true);
                        placeholderButton.GetComponentInChildren<Text>().text = ("Lagerplatz " + "R" + row + "C" + column);
                    }
                    else
                    {
                        placeHolder = shelfObject.transform.Find(("Lagerplatz " + "R" + row + "C" + column)).gameObject;
                        GameObject button = (GameObject)Instantiate(placeholderButton);
                        //set the created object as child from the parent
                        button.transform.SetParent(placeholderButton.transform.parent);
                        //shift the new button below the current button
                        button.GetComponentInChildren<Text>().text = placeHolder.name;
                    }
                }
            }
            //setzt die vertikale ScrollviewReihe auf die Anzahl der Regalreihen
            //set the vertical Scrollview list to the number of shelf rows
            placeholderButton.transform.parent.GetComponent<GridLayoutGroup>().constraintCount = shelfPH.rowCount;
        }
        else
        {
            Debug.Log("here");
            shelfButton.GetComponentInChildren<Text>().text = "Kein Regal ausgewählt";
        }

    }

    //TODO: Improve: give every button a direkt reference to the object
    //search the placeholder with the name of the button
    public void ButtonGetPlaceholder(string buttonText)
    {
        GameObject shelfObject = ignoreRaycastPlatzhalter.go;
        GameObject go = shelfObject.transform.Find(buttonText).gameObject;

        //check if the placehlder is already shown
        if (go.layer == 10 && go != ignoreRaycastPlatzhalter.go)
        {
            ResetLayers(10);
            ignoreRaycastPlatzhalter = new IgnoreCast(go, go.layer);
            go.layer = 2;
            platzhalterObject = "<color=#32CD32>" + go.name + "</color>\n";
        }
    }

    //TODO: Get Event and the name of the button who fired
    public void ButtonGetPlaceholder(GameObject go)
    {

    }

    //show the number of filled placeholders and number of all placeholder
    public void ButtonGetShelfVolume()
    {
        PlaceholderGenerator shelfObject = ignoreRaycastRegal.go.GetComponentInChildren<PlaceholderGenerator>();
        string shelfVolume = "- Max Storage Count: " + "<color=#32CD32>" + shelfObject.GetStorageCount() + "</color>\n" + " of which are filled " + "<color=#FFFF00>" + shelfObject.GetFilledStorageCount() + "</color>";
        textUI.text = shelfObject.name + "\n" + shelfVolume;
    }
}

internal class IgnoreCast
{
    public GameObject go;
    public int layerSave;

    public IgnoreCast(GameObject go, int layerSave)
    {
        this.go = go;
        this.layerSave = layerSave;
    }
}